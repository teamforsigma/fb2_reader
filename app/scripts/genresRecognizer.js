/**
 * Created by Constantine on 02.11.2014.
 */

angular.module('genreRecognizer', [])

    .service('parseGenre', function($http) {

        this.getGenreNameEn = function(key) {
            for (var i = 0; i < genres.length; i++) {
                if (genres[i].Key === key) {
                    return genres[i].En;
                }
            }
            return key;
        };

        this.getGenreKeyEn = function(name) {
            for (var i = 0; i < genres.length; i++) {
                if (genres[i].En === name) {
                    return genres[i].Key;
                }
            }
            return name;
        };


    });