/**
 * Created by Constantine on 20.10.2014.
 */

angular.module('bookModule', ['ionic', 'currentBookTracker', 'settingsHolder',
    'text.selection', 'utils', 'popup.context', 'bookmarkManager', 'parameters', 'popup.addBookmark',
    'notesManager', 'popup.gotoNext'])

    .controller('BookController', function ($scope, $rootScope, $ionicGesture, $ionicSlideBoxDelegate, $ionicPopup, currentBook,
                                            htmlTextPreparer, selectionHandler, textContextPopup, bookmarks, notes, windowSize,
                                            bookmarkAddPopup, $state, $stateParams, windowParams, gotoNextPopup, $timeout, userSettings) {

        $scope.winParams = windowParams;

        $scope.pagesModel = ["", "", ""];
        var book;
        var page;

        $scope.activePage = 1;

        $scope.selectionHandler = selectionHandler;

        var dir = 0;
        var lastIndex = 1;

        var zoomingElement;

        $rootScope.$on('setBook', function (book) {
            $scope.init();
        });

        $scope.NextPage = function () {
            $ionicSlideBoxDelegate.next();
            //change index?
        };
        $scope.PreviousPage = function () {
            $ionicSlideBoxDelegate.previous();
            //change index?
        };
        $scope.GetTotalPages = function () {
            $scope.totalPages = $scope.pages.length; //service or just leave like this

        };
        $scope.GetPages = function () {
            $scope.pages = currentBook.getPages();
        };

        var getPage = function (index) {
            "use strict";
            var pageText;
            if (index >= $scope.pages.length) {
                pageText = $scope.pages[0].pageText;
            } else if (index < 0) {
                pageText = $scope.pages[$scope.pages.length - 1].pageText;
            } else {
                pageText = $scope.pages[index].pageText;
            }
            pageText = htmlTextPreparer.prepareForSelection(pageText);
            return pageText;
        };

        $scope.saveBookmark = function (name) {
            var currentPage = $scope.pages[$scope.currentPageIndex];
            var tagId = $(currentPage.pageText).first().attr('id').substring(7); //skip section word
            bookmarks.saveBookmark(tagId, name).then(function (obj) {
//                $scope.bookmarksList.push(obj);
                //maybe success dialog
            });
        };

        $scope.saveNote = function (noteObj) {
            notes.saveNote(noteObj).then(function (obj) {
//                $scope.notesList.push(obj);
                console.log('pushed in notes list');
                //maybe success dialog
            }, function () {
                console.log('rejected in ctrl.saveNote');
            });
        };

        $scope.addBookmarkPS = function () {
            bookmarkAddPopup.show($scope);
        };

        $scope.gotoNextPS = function () {
            gotoNextPopup.show($scope);
        };

        $scope.onPageChanged = function (index) {
            "use strict";
            if (lastIndex === 2 && index === 0) {
                dir = -1;
            } else if (lastIndex === 0 && index === 2) {
                dir = 1;
            } else {
                dir = (lastIndex - index);
                dir = dir > 0 ? 1 : -1;
            }

            $scope.currentPageIndex += -dir;

            currentBook.updateCurrentPage($scope.currentPageIndex);

            if ($scope.currentPageIndex >= $scope.pages.length) {
                $scope.currentPageIndex = 0;
            } else if ($scope.currentPageIndex < 0) {
                $scope.currentPageIndex = $scope.pages.length - 1;
            }

            lastIndex = index;
            switch (index) {
                case 0:
                    // Right to left
                    if (dir > 0) {
                        $scope.pagesModel[2] = getPage($scope.currentPageIndex - 1);
                    } else {
                        $scope.pagesModel[1] = getPage($scope.currentPageIndex + 1);
                    }
                    break;
                case 1:
                    if (dir > 0) {
                        $scope.pagesModel[0] = getPage($scope.currentPageIndex - 1);
                    } else {
                        $scope.pagesModel[2] = getPage($scope.currentPageIndex + 1);
                    }
                    break;
                case 2:
                    if (dir > 0) {
                        $scope.pagesModel[1] = getPage($scope.currentPageIndex - 1);
                    } else {
                        $scope.pagesModel[0] = getPage($scope.currentPageIndex + 1);
                    }
                    break;
            }
            selectionHandler.setPage($('#slide' + index));
            selectionHandler.hide();
            addZooming();
        };

        var addZooming = function () {
            var cPage = $('#slide' + lastIndex);
            if (zoomingElement !== undefined) {
                zoomingElement.panzoom("reset");
                zoomingElement.panzoom("destroy");
            }
            cPage.panzoom({
                minScale: 1,
                maxScale: 3,
                disablePan: true,
                contain: "invert",
                stopEventPropagation: false,
                onStart: function (e, panzoom) {
                    selectionHandler.setEnabled(false);
                    $scope.$apply(function () {
                        selectionHandler.hide();
                        $scope.setFullScreenMode(true);
                    });
                    panzoom.option("disablePan", false);
                    panzoom.option("stopEventPropagation", true);
                },
                onEnd: function (e, panzoom, matrix) {
                    if (matrix[0] < 1.02 && matrix[3] < 1.02) {
                        // Zoomed out to normal view
                        panzoom.reset();
                        panzoom.option("stopEventPropagation", false);
                        panzoom.option("disablePan", true);
                        selectionHandler.setEnabled(true);
                    }
                }
            });
            zoomingElement = cPage;
        };

        var updateOrientation = function (e) {
            selectionHandler.hide();
        };

        $scope.init = function () {
            window.addEventListener("orientationchange", updateOrientation);
            selectionHandler.setScope($scope);
            book = currentBook.getBook();
            if ($stateParams.caller === 'notesAndBookmarks' || $stateParams.caller === 'gotoPage') {
                page = parseInt($stateParams.startIndex);
                currentBook.updateCurrentPage(page);
            } else {
                page = book === undefined ? 0 : book.lastReadPage;
            }
            $scope.currentPageIndex = page;

            $scope.GetPages();
            $scope.GetTotalPages();

            if ($scope.pages !== undefined && $scope.pages.length > 0) {
                $scope.pagesModel[1] = getPage($scope.currentPageIndex);
                $scope.pagesModel[0] = getPage($scope.currentPageIndex - 1);
                $scope.pagesModel[2] = getPage($scope.currentPageIndex + 1);
            }
            addZooming();
            // Run set page after all DOM changes
            $timeout(function () {
                selectionHandler.setPage($('#slide1'));
            });
        };

        $scope.onSelectionImgLoad = function () {
            "use strict";
            console.log("Selection images loaded");
            textContextPopup.init($scope);
            selectionHandler.init($ionicGesture, windowSize);
        };

        var selectionVisible = false;

        $scope.onSlideClick = function () {
            "use strict";
            if (selectionVisible) {
                selectionHandler.hide();
            } else {
                $scope.changeFullScreenMode();
            }
        };

        $scope.$on('selectionStart', function (event) {
            $scope.setFullScreenMode(true);
            $ionicSlideBoxDelegate.enableSlide(false);
            selectionVisible = true;

        });
        $scope.$on('selectionEnd', function (event) {
            $ionicSlideBoxDelegate.enableSlide(true);
            selectionVisible = false;
        });

        $scope.$on('destroy', function () {
            window.removeEventListener("orientationchange", updateOrientation);
        });

        $scope.init();

        $scope.textSettings = userSettings.getSettings();
    })

.filter('rightColor', function(userSettings) {
    return function(pageText) {
        var finalPageText = "";
        var elements = $(pageText);
        elements.each(function (index, element) {
            try {
                $(element).find('span').css('color', userSettings.getSettings().textColor.toString());
            } catch (err) {
                console.log("ERROR: " + err);
            }
            finalPageText += element.outerHTML;
        });
        return finalPageText;
    }
});