angular.module('bookmarksModule', ['ionic', 'bookmarkManager', 'currentBookTracker'])
    .controller('BookmarksController', function ($scope, $state, currentBook, bookmarks) {
        $scope.bookmarksList = [];
        var getBookmarks = function () {
            if (currentBook.getBook() !== undefined) {
                bookmarks.getBookmarks().then(function (result) {
                    $scope.bookmarksList = result;
                });
            }
        };
        $scope.deleteBookmark = function (bookmark) {
            bookmarks.deleteBookmark(bookmark).then(function () {
                $scope.bookmarksList.splice($scope.bookmarksList.indexOf(bookmark), 1);
                // maybe a dialog
            });
        };
        $scope.goToBookmark = function (bookmark) {
            var pageIndex = bookmarks.findBookmark(bookmark);
            $state.go('menu.book', { startIndex: pageIndex, caller: 'notesAndBookmarks'});
        };

        getBookmarks();
    });

