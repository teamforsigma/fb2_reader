angular.module('fileExplorerModule', ['ionic', 'utils.fs'])
    .controller('FileExplorerController', function ($scope, $state, $rootScope, $ionicScrollDelegate, fs, newBookService) {

        var root;
        var parentDir;
        var currentDir;
        var lastDir;

//        var extensions = $stateParams.extensions;

        var extensions = ['fb2'];

        $scope.fsItems = [];

        $scope.onItemClick = function (fsItem) {
            if (fsItem.isDirectory) {
                listDir(fsItem);
            } else if (fsItem.isFile) {
                $state.go('menu.libraryTabs.title');
                newBookService.addNewBook(fsItem);
            }
        };

        $scope.up = function () {
            listDir(parentDir);
        };

        $scope.back = function () {
            if (lastDir !== undefined) {
                listDir(lastDir);
            }
        };

        $scope.home = function () {
            listDir(root);
        };

        var getFileExtension = function (name) {
            return (/[.]/.exec(name)) ? /[^.]+$/.exec(name) : undefined;
        };

        var listDir = function (dir) {
            lastDir = currentDir;

            currentDir = dir;

            dir.getParent(function (parent) {
                parentDir = parent;
            }, function (err) {
                console.log("Get parent error: " + err.code);
            });

            var dirReader = dir.createReader();

            dirReader.readEntries(function (entries) {
                $scope.$apply(function () {
                    var fsItems = [];
                    $.each(entries, function (index, entry) {
                        if (entry.isFile) {
                            var extension = getFileExtension(entry.name);
                            if (extension === undefined || $.inArray(extension[0], extensions) === -1) {
                                return;
                            }
                        }
                        fsItems.push(entry);
                    });
                    $scope.fsItems = fsItems;
                    $ionicScrollDelegate.scrollTop();
                });
            }, function (err) {
                console.log("Error reading dir entries: " + err.code);
            });

        };

        fs.get(LocalFileSystem.PERSISTENT).then(function (fileSystem) {
            root = fileSystem.root;
            listDir(root);
        });
    });
