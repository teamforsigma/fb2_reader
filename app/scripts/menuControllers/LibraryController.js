/**
 * Created by Constantine on 20.10.2014.
 */

angular.module('libraryModule', ['ionic', 'currentBookTracker', 'settingsHolder',
    'text.selection', 'utils', 'popup.context', 'bookmarkManager', 'parameters', 'popup.addBookmark', 'libraryDataHolder', 'currentBookTracker',
    'fb2.fb2Reader', 'utils.fs.reader', 'fileExplorerModule', 'popup.bookOptions', 'genreRecognizer'])

    .controller('LibraryController', function ($scope) {
        $scope.openDropbox = function () {
            navigator.startApp.check("com.dropbox.android", function (message) { /* success */
                    console.log(message); // => OK
                    navigator.startApp.start("com.dropbox.android", function (message) { /* success */
                            console.log(message); // => OK
                        },
                        function (error) { /* error */
                            console.log('', error);
                        });
                },
                function (error) { /* error */
                    console.log('', error);
                });
        };
    })

    .controller('TitleTabController', function ($scope, $rootScope, $state, $stateParams, $q, currentBook, bookOptionsPopup, fb2Reader, fileMover, libraryData, parseGenre, $ionicLoading) {
        $scope.pageTitle = '';

        $scope.query = '';
        $scope.books = [];

        //check is there a caller
        var fillBooks = function () {
            //check is there a caller
            if ($stateParams.isAuthor == "true") {
                $scope.books = libraryData.getBooksShortInfoByAuthorID($stateParams.text);
                if ($scope.books.length > 0) {
                    $scope.pageTitle = $scope.books[0].authorName;
                } else {
                    console.log("page library author book - error");
                }
            } else {
                if ($stateParams.isAuthor == "false") {
                    $scope.books = libraryData.getBooksShortInfoByGenre($stateParams.text);
                    if ($scope.books.length > 0) {
                        $scope.pageTitle = parseGenre.getGenreNameEn($scope.books[0].bookGenre);
                    } else {
                        console.log("page library title genre book - error");
                    }
                } else {
                    $scope.books = libraryData.getBooksShortInfo();
                    $scope.pageTitle = 'Library';
                }
            }
        };

        fillBooks();

        //Opens popup with book options
        $scope.openOptions = function (book) {
            bookOptionsPopup.show($scope, book)
        };

        //Deleting book
        $scope.deleteBook = function (book) {
            return libraryData.deleteBookByID(book.id).then(function () {
                $scope.books = [];
                fillBooks();
            });
        };

        //Sets new rating to a book
        $scope.updateBookRate = function (bookID, newRate) {
            libraryData.updateBookRate(bookID, newRate);
            $scope.books = [];
            fillBooks();
        };


        //For opening after selecting
        $scope.openBookFromLibrary = function (book) {
            $ionicLoading.show({
                template: '<h2 class="icon ion-loading-c"></h2>',
                animation: 'fade-in',
                noBackdrop: false
            });

            currentBook.setBookLocal(libraryData.getBookById(book.id)).then(function () {
                $ionicLoading.hide();
                $state.go('menu.book', { 'caller': 'library' });
            });
        };

        //For deleting search querry
        $scope.clearQuery = function () {
            this.query = '';
        };

        var onOpenStart = function () {
            $ionicLoading.show({
                template: '<h2 class="icon ion-loading-c"></h2>',
                animation: 'fade-in',
                noBackdrop: false
            });
        };

        var onOpenEnd = function () {
            fillBooks();
            $ionicLoading.hide();
        };

        $rootScope.$on("openBookStart", onOpenStart);
        $rootScope.$on("openBookEnd", onOpenEnd)
    })


    .controller('AuthorTabController', function ($scope, $state, $stateParams, $q, libraryData) {
        $scope.query = '';
        $scope.books = libraryData.getBooksShortInfo();

        //For deleting search query
        $scope.clearQuery = function () {
            this.query = '';
        };
    })

    .controller('GenreTabController', function ($scope, libraryData, parseGenre) {
        $scope.query = '';
        $scope.books = libraryData.getBooksShortInfo();

        for (var i = 0; i < $scope.books.length; i++) {
            $scope.books[i].genreName = parseGenre.getGenreNameEn($scope.books[i].bookGenre);
        }

        //For deleting search query
        $scope.clearQuery = function () {
            this.query = '';
        };
    })

    .filter('unique', function () {

        return function (items, filterOn) {

            if (filterOn === false) {
                return items;
            }

            if ((filterOn || angular.isUndefined(filterOn)) && angular.isArray(items)) {
                var hashCheck = {}, newItems = [];

                var extractValueToCompare = function (item) {
                    if (angular.isObject(item) && angular.isString(filterOn)) {
                        return item[filterOn];
                    } else {
                        return item;
                    }
                };

                angular.forEach(items, function (item) {
                    var valueToCheck, isDuplicate = false;

                    for (var i = 0; i < newItems.length; i++) {
                        if (angular.equals(extractValueToCompare(newItems[i]), extractValueToCompare(item))) {
                            isDuplicate = true;
                            break;
                        }
                    }
                    if (!isDuplicate) {
                        newItems.push(item);
                    }

                });
                items = newItems;
            }
            return items;
        }
    })

    .filter('searchAll', function () {
        return function (input, query) {
            if (query === '')
                return input;
            var wordsQuery = [];
            wordsQuery[0] = '';
            var wordsNum = 0;
            for (var i = 0; i < query.length; i++) {
                if (query.charAt(i) === ' ') {
                    wordsNum++;
                    wordsQuery[wordsNum] = '';
                } else {
                    wordsQuery[wordsNum] = wordsQuery[wordsNum] + query.charAt(i);
                }
            }
            wordsNum++;
            var out = [];
            var outNum = 0;
            for (var j = 0; j < input.length; j++) {
                var shouldAdd = true;
                for (var i = 0; i < wordsNum; i++) {
                    if ((input[j].authorName.toLowerCase().indexOf(wordsQuery[i].toLowerCase()) === -1) && (input[j].bookName.toLowerCase().indexOf(wordsQuery[i].toLowerCase()) === -1)) {
                        shouldAdd = false;
                        break;
                    }
                }
                if (shouldAdd) {
                    out[outNum] = input[j];
                    outNum++;
                }
            }

            return out;
        };
    })

    .filter('searchAuthor', function () {
        return function (input, query) {
            if (query === '')
                return input;
            var wordsQuery = [];
            wordsQuery[0] = '';
            var wordsNum = 0;
            for (var i = 0; i < query.length; i++) {
                if (query.charAt(i) === ' ') {
                    wordsNum++;
                    wordsQuery[wordsNum] = '';
                } else {
                    wordsQuery[wordsNum] = wordsQuery[wordsNum] + query.charAt(i);
                }
            }
            wordsNum++;
            var out = [];
            var outNum = 0;
            for (var j = 0; j < input.length; j++) {
                var shouldAdd = true;
                for (var i = 0; i < wordsNum; i++) {
                    if (input[j].authorName.toLowerCase().indexOf(wordsQuery[i].toLowerCase()) === -1) {
                        shouldAdd = false;
                        break;
                    }
                }
                if (shouldAdd) {
                    out[outNum] = input[j];
                    outNum++;
                }
            }

            return out;
        };
    })

    .filter('searchGenre', function () {
        return function (input, query) {
            if (query === '')
                return input;
            var wordsQuery = [];
            wordsQuery[0] = '';
            var wordsNum = 0;
            for (var i = 0; i < query.length; i++) {
                if (query.charAt(i) === ' ') {
                    wordsNum++;
                    wordsQuery[wordsNum] = '';
                } else {
                    wordsQuery[wordsNum] = wordsQuery[wordsNum] + query.charAt(i);
                }
            }
            wordsNum++;
            var out = [];
            var outNum = 0;
            for (var j = 0; j < input.length; j++) {
                var shouldAdd = true;
                for (var i = 0; i < wordsNum; i++) {
                    if (input[j].bookGenre.toLowerCase().indexOf(wordsQuery[i].toLowerCase()) === -1) {
                        shouldAdd = false;
                        break;
                    }
                }
                if (shouldAdd) {
                    out[outNum] = input[j];
                    outNum++;
                }
            }

            return out;
        };
    })

