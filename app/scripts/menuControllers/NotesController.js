angular.module('notesModule', ['ionic', 'notesManager', 'currentBookTracker'])
    .controller('NotesController', function ($scope, $state, currentBook, notes) {
        $scope.notesList = [];

        var getNotes = function () {
            if (currentBook.getBook() !== undefined) {
                notes.getNotes().then(function (result) {
                    $scope.notesList = result;
                });
            }
        };
        $scope.goToNote = function (note) {
            var pageIndex = notes.findNote(note);
            $state.go('menu.book', { startIndex: pageIndex, caller: 'notesAndBookmarks'});
        };

        getNotes();
    });

