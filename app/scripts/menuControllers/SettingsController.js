/**
 * Created by Constantine on 20.10.2014.
 */

angular.module('settingsModule', ['ionic', 'currentBookTracker', 'settingsHolder',
    'text.selection', 'utils', 'popup.context', 'bookmarkManager', 'parameters', 'popup.addBookmark', 'currentBookTracker'])

    .controller('SettingsController', function($scope, userSettings, currentBook) {

        $scope.avaliableFontSizes = [];
        for (var i = 12; i < 31; i++) {
            $scope.avaliableFontSizes.push({number: i, px: i.toString()+"px"});
        }

        $scope.avaliableFontFamilies = [{name: "Times serif", value: "times, serif" },
            {name :"Andale Mono", value: "AndaleMono, monospace"},
            {name : "Arial Black", value : "Gadget, sans-serif"},
            {name : "Palatino Linotype", value : "Palatino, serif"}];

        $scope.availableTextColorSamples = [{name : "red", hex : "#ff0000"}, {name : "green", hex : "#00ff00"},
            {name : "blue", hex : "#0000ff"}, {name : "purple", hex : "#ff00ff"}, {name : "yellow", hex: "#00ffff"},
            {name: "orange", hex: "#ffff00"}, {name:"white", hex:"#ffffff"}, {name:"black", hex:"#000000"}];

        $scope.availableBackColorSamples = [{name : "red", hex : "#ff0000"}, {name : "green", hex : "#00ff00"},
            {name : "blue", hex : "#0000ff"}, {name : "purple", hex : "#ff00ff"}, {name : "yellow", hex: "#00ffff"},
            {name: "orange", hex: "#ffff00"}, {name:"white", hex:"#ffffff"}, {name:"black", hex:"#000000"}];

        $scope.acceptChanges = function(fontSize,fontFamily,backColor,textColor) {
            var fontChanged = true;
            var settings = userSettings.getSettings();
            if (fontSize.number === settings.fontSize.number && fontFamily.name === settings.fontFamily.name)
                fontChanged = false;
            userSettings.setTextColor(textColor);
            userSettings.setBackColor(backColor);
            userSettings.setFontSize(fontSize);
            userSettings.setFontFamily(fontFamily);
            userSettings.saveData();
            if (fontChanged)
                currentBook.rescaleText();
        };

        $scope.listTextSamplesChange = function(selectedSample) {
            console.log("in $scope.listTextSamplesChange");
            console.log(selectedSample);
            if (selectedSample !== undefined && selectedSample !== null) {
                $scope.selectedTextColor = selectedSample.hex;
                console.log("Changed selectedTextColor to " + selectedSample.hex);
            }
        }

        $scope.listBackSamplesChange = function(selectedSample) {
            console.log("in $scope.listBackSamplesChange");
            console.log(selectedSample);
            if (selectedSample !== undefined && selectedSample !== null) {
                $scope.selectedBackColor = selectedSample.hex;
                console.log("Changed selectedBackColor to " + selectedSample.hex);
            }
        }

        $scope.resetChanges = function() {
            userSettings.loadData();
            $scope.selectedTextColor = userSettings.getSettings().textColor;
            $scope.selectedBackColor = userSettings.getSettings().backColor;
            $scope.selectedTextColorSample = "";
            $scope.selectedBackColorSamloe = "";

            for (var i =0; i < $scope.availableTextColorSamples.length; i++) {
                if ($scope.selectedTextColor === $scope.availableTextColorSamples[i].hex)
                    $scope.selectedTextColorSample = $scope.availableTextColorSamples[i];
                if ($scope.selectedBackColor === $scope.availableBackColorSamples[i].hex)
                    $scope.selectedBackColorSample = $scope.availableBackColorSamples[i];
            }

            console.log("SettingsControl selected samples text " + $scope.selectedTextColorSample.name + " back " + $scope.selectedBackColorSample.name);

            $scope.indexFamily = -1;
            for (var i = 0; i < $scope.avaliableFontFamilies.length; i++) {
                if ($scope.avaliableFontFamilies[i].name === userSettings.getSettings().fontFamily.name)
                    $scope.indexFamily = i;
            }
            $scope.selectedFontFamily = $scope.avaliableFontFamilies[$scope.indexFamily];

            $scope.indexSize = -1;
            for (var i = 0; i < $scope.avaliableFontSizes.length; i++) {
                if ($scope.avaliableFontSizes[i].number === userSettings.getSettings().fontSize.number)
                    $scope.indexSize = i;
            }

            $scope.selectedFontSize = $scope.avaliableFontSizes[$scope.indexSize];
        };
        $scope.resetChanges();


    })