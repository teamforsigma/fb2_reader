/**
 * Created by Constantine on 20.10.2014.
 */

angular.module('recentModule', ['ionic', 'currentBookTracker', 'settingsHolder',
    'text.selection', 'utils', 'popup.context', 'bookmarkManager', 'parameters', 'currentBookTracker', 'popup.bookOptions', 'libraryDataHolder', 'popup.addBookmark'])

    .controller('RecentController', function($scope, $state, $stateParams, currentBook, libraryData, bookOptionsPopup, $ionicLoading) {
        $scope.query = '';
        $scope.books = [];

        $scope.books = libraryData.getBooksRecentInfo($stateParams.text);

        //Opens popup with book options
        $scope.openOptions = function (book) {
            bookOptionsPopup.show($scope, book)
        };

        //Deleting book
        $scope.deleteBook = function (book) {
            return libraryData.deleteBookByID(book.id).then(function () {
                $scope.books = [];
                $scope.books = libraryData.getBooksRecentInfo($stateParams.text);
            });
        };

        //Sets new rating to a book
        $scope.updateBookRate = function (bookID, newRate) {
            libraryData.updateBookRate(bookID, newRate);
            $scope.books = [];
            $scope.books = libraryData.getBooksRecentInfo($stateParams.text);
        };


        //For opening after selecting
        $scope.openBookFromLibrary = function (book) {
            $ionicLoading.show({
                template: '<h2 class="icon ion-loading-c"></h2>',
                animation: 'fade-in',
                noBackdrop: false
            });

            currentBook.setBookLocal(libraryData.getBookById(book.id)).then(function () {
                $ionicLoading.hide();
                $state.go('menu.book', { 'caller': 'library' });
            });
        };

        //For deleting search querry
        $scope.clearQuery = function () {
            this.query = '';
        };

    })


.filter('searchAll', function () {
    return function (input, query) {
        if (query === '')
            return input;
        var wordsQuery = [];
        wordsQuery[0] = '';
        var wordsNum = 0;
        for (var i = 0; i < query.length; i++) {
            if (query.charAt(i) === ' ') {
                wordsNum++;
                wordsQuery[wordsNum] = '';
            } else {
                wordsQuery[wordsNum] = wordsQuery[wordsNum] + query.charAt(i);
            }
        }
        wordsNum++;
        var out = [];
        var outNum = 0;
        for (var j = 0; j < input.length; j++) {
            var shouldAdd = true;
            for (var i = 0; i < wordsNum; i++) {
                if ((input[j].authorName.toLowerCase().indexOf(wordsQuery[i].toLowerCase()) === -1) && (input[j].bookName.toLowerCase().indexOf(wordsQuery[i].toLowerCase()) === -1)) {
                    shouldAdd = false;
                    break;
                }
            }
            if (shouldAdd) {
                out[outNum] = input[j];
                outNum++;
            }
        }

        return out;
    };
})