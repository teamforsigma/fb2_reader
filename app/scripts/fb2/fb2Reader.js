angular.module('fb2.fb2Reader', ['utils.fs.reader', 'fb2.parser'])
    .service('fb2Reader', function ($q, externalFileReader, fb2Parser) {
        'use strict';
        this.read = function (filePath) {
            return externalFileReader.readAsText(filePath, 'UTF8').then(function (text) {
                var fileName;
                if (typeof filePath === 'string') {
                    fileName = filePath.substr(filePath.lastIndexOf('/') + 1);
                } else {
                    fileName = filePath.name;
                }
                var str = text.substr(0, text.indexOf("\n"));
                var matches = str.match('encoding="(.*?)"');
                if (matches !== 0) {
                    var encoding = matches[1];
                    text = null;
                    return externalFileReader.readAsText(filePath, encoding)
                        .then(fb2Parser.parseFb2).then(function (book) {
                            book.encoding = encoding;
                            book.fileName = fileName;
                            return book;
                        });
                } else {
                    return fb2Parser.parseFb2(text).then(function (book) {
                        book.encoding = 'UTF8';
                        book.fileName = fileName;
                        return book;
                    });
                }
            });
        };
    });