angular.module('fb2.parser', ['fb2.parser.processors'])
    .service('fb2Parser', function (schemaParser, fb2Schema) {
        /**
         * Parse raw fb2 file xml into js object
         * @param text Raw fb2 xml text
         * @returns {*} Js object which represents fb2 book
         */
        this.parseFb2 = function (text) {
            return schemaParser.parseXmlWithSchema(text, fb2Schema);
        };
    })
    .service('schemaParser', function ($q) {
        'use strict';

        function xmlAttrNameToJsName(attrName) {
            var result = '';
            var len = attrName.length;
            for (var i = 0; i < len; i++) {
                if (attrName[i] === '-') {
                    while (attrName[i] === '-' && i != len) {
                        i++;
                    }
                    if (i != len) {
                        result += attrName[i].toUpperCase();
                    }
                } else {
                    result += attrName[i];
                }
            }
            return result;
        }

        this.parseXmlWithSchema = function (xml, schema) {
            var resultObject = {};
            parse(schema, resultObject, $($.parseXML(xml)));
            return $q.when(resultObject);
            function parse(parseSchema, objElement, jElement) {
                $.each(parseSchema, function (key, value) {
                    var name = xmlAttrNameToJsName(key);
                    if (value.array) {
                        var elements = jElement.find(key);
                        if (elements !== null) {
                            var arr = [];
                            objElement[name] = arr;
                            elements.each(function (index, element) {
                                element = $(element);
                                if (value.processor !== undefined) {
                                    // Assume we process only leaf elements
                                    arr.push(value.processor(resultObject, element));
                                } else {
                                    if (value.content !== undefined) {
                                        var obj = {};
                                        arr.push(obj);
                                        parse(value.content, obj, element);
                                    } else {
                                        arr.push(element.text());
                                    }
                                }
                            });
                        }
                    } else {
                        var element = jElement.find(key);
                        if (element !== null) {
                            if (value.processor !== undefined) {
                                objElement[name] = value.processor(resultObject, element);
                            } else {
                                if (value.content !== undefined) {
                                    objElement[name] = {};
                                    parse(value.content, objElement[name], element);
                                } else {
                                    objElement[name] = element.text();
                                }
                            }
                        }
                    }
                });
            }
        };
    })
    .factory('fb2Schema', function (annotationProcessor, binaryProcessor, bodyProcessor, keywordProcessor, coverpageProcessor) {
        'use strict';
        var authorSchema = {
            "first-name": {},
            "middle-name": {},
            "last-name": {},
            "nickname": {},
            "home-page": {array: true},
            "email": {array: true},
            "id": {}
        };

        var bookInfo = {
            "book-title": {},
            "lang": {},
            "src-lang": {},
            "date": {},

            "genre": { array: true},
            "author": {
                array: true,
                content: authorSchema
            },
            "translator": {
                array: true,
                content: authorSchema
            },
            "annotation": {
                processor: annotationProcessor
            },
            "keywords": {
                processor: keywordProcessor
            },
            "sequence": {},
            "coverpage": {
                processor: coverpageProcessor
            }

        };

        var parseSchema = {
            "description": {
                content: {
                    "title-info": {
                        content: bookInfo
                    },
                    "src-title-info": {
                        content: bookInfo
                    },
                    "document-info": {
                        content: {
                            "author": {
                                array: true,
                                content: authorSchema
                            },
                            "program-used": {},
                            "date": {},
                            "src-url": {array: true},
                            "src-ocr": {array: true},
                            "id": {},
                            "version": {},
                            "history": {},
                            "publisher": {array: true}  // Todo: Find if object
                        }
                    },
                    "publish-info": {
                        content: {
                            "book-name": {},
                            "publisher": {},
                            "city": {},
                            "year": {},
                            "isbn": {},
                            "sequence": {}
                        }
                    },
                    "custom-info": {}
                }
            },
            "binary": {
                array: true,
                processor: binaryProcessor
            },
            "body": {
                array: true,
                processor: bodyProcessor
            }
        };

        return parseSchema;
    });

angular.module('fb2.parser.processors', [])
    .factory('keywordProcessor', function () {
        "use strict";
        var process = function (book, keywords) {
            return keywords.text().split(',');
        };
        return process;
    })
    .factory('coverpageProcessor', function () {
        "use strict";
        var process = function (book, coverpage) {
            var image = coverpage.find("image");
            var name;
            if (image.length !== 0) {
                $.each(image[0].attributes, function (index, value) {
                    if (value.localName === "href") {
                        name = value.value.substr(1);
                    }
                });
            }
            return name;
        };
        return process;
    })
    .factory('annotationProcessor', function (bodyProcessor) {
        "use strict";
        return bodyProcessor;
    })
    .factory('binaryProcessor', function () {
        'use strict';
        var process = function (book, binary) {
            var binaryObj = {};
            binaryObj.id = binary.attr('id');
            binaryObj.contentType = binary.attr('content-type');
            binaryObj.data = binary.text();
            return binaryObj;
        };
        return process;
    })
    .factory('bodyProcessor', function () {
        'use strict';
        var process = function (book, body) {
            var elementCount = 0;

            function sectionHtml(src, _level, isNotes) {
                var html = "";
                var level = _level + 2;
                if (level > 6) {
                    level = 6;
                }
                elementCount += 1;
                $(src).children().each(function () {
                    elementCount += 1;
                    switch ((this).tagName) {
                        case 'image':
                            var id = "";
                            $.each(this.attributes, function (index, value) {
                                if (value.localName === "href") {
                                    //Todo: Image can be inplace, not in binary section
                                    id = value.value.substr(1);
                                }
                            });

                            var bData;
                            $.each(book.binary, function (pos, binary) {
                                if (binary.id === id) {
                                    bData = binary.data;
                                }
                            });
                            html += "<div id='section" + elementCount + "' class='bookimage'><img class='section-illustration' id=" + id + " src='data:image/gif;base64," + bData + "' /></div>";
                            break;
                        case 'title':
                            if (isNotes && _level > 0) {
                                $(this).children("p").each(function () {
                                    html += "<a id='" + $(src).attr("id") + "'><h" + level + " class='section-title'>" + $(this).text() + "</h" + level + "></a>";
                                });

                            } else {
                                $(this).children("p").each(function () {
                                    html += "<h" + level + " id='section" + elementCount + "' class='section-title'>" + $(this).text() + "</h" + level + ">";
                                });
                            }
                            break;
                        case 'epigraph':
                        {

                            $(this).children().each(function () {
                                switch ((this).tagName) {
                                    case 'p':
                                        html += "<p id='section" + elementCount + "' class='section-epigraph'>" + $(this).text() + "</p>";
                                        break;
                                    case 'text-author':
                                        html += "<p id='section" + elementCount + "' class='section-epigraph-author'>" + $(this).text() + "</p><br/>";
                                        break;
                                    case 'empty-line':
                                        html += "<br/>";
                                        break;
                                }
                            });
                            break;
                        }
                        case 'section':
                            html += sectionHtml($(this), _level + 1, isNotes);
                            break;
                        case 'p':
                            if ($(this).children("a").size() > 0) {

                                $(this).children("a").each(function () {
                                    elementCount += 1;
                                    var href = "";
                                    $.each(this.attributes, function (index, value) {
                                        if (value.localName === "href") {
                                            href = value.value;
                                        }
                                    });

                                    $(this).text(" <a id='section" + elementCount + "' href='" + href + "'>" + $(this).text() + "</a>");
                                });

                                html += "<p id='section" + elementCount + "' class='body-paragpraph'>" + $(this).text() + "</p>";
                            } else {
                                html += "<p id='section" + elementCount + "' >" + $(this).text() + "</p>";
                            }
                            break;
                        case 'poem':
                        {
                            html += "<p id='section" + elementCount + "' class='poem'>";
                            $(this).children().each(function () {
                                elementCount += 1;
                                switch ((this).tagName) {
                                    case 'title':
                                        html += "<h1 id='section" + elementCount + "' class='poem-title'>" + $(this).text() + "</h1>";
                                        break;
                                    case 'epigraph':
                                        html += "<p id='section" + elementCount + "' class='poem-epigraph'>" + $(this).text() + "</p>";
                                        break;
                                    case 'stanza':
                                        html += "<p id='section" + elementCount + "' class='poem-stanza'>";
                                        $(this).children("v").each(function () {
                                            elementCount++;
                                            html += "<p id='section" + elementCount + "'>" + $(this).text() + "</p>";
                                        });
                                        html += "</p>";
                                        break;
                                    case 'empty-line':
                                        html += "<br/>";
                                        break;
                                }
                            });

                            html += "</p>";
                        }
                            break;
                        case 'cite':

                            break;
                        case 'table':
                            html += this;
                            break;
                    }
                });
                return html;
            }

            return sectionHtml(body, 0, false);
        };
        return process;
    });

