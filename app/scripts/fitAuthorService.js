/**
 * Created by Home on 11/3/2014.
 */

angular.module('fitAuthorService', ['nameComparer'])

    .service('fitAuthorService', function(nameComparer) {

        this.getRelevantAuthors = function(originalAuthor, authors) {
            var distances = [];

            for (var i = 0; i < authors.length; i++) {
                if (originalAuthor.authorId === authors[i].authorId) {
                    continue;
                }
                var dist = nameComparer.compare(originalAuthor, authors[i]);
                if (dist > 6 || dist > (originalAuthor.firstName.length + originalAuthor.lastName.length) / 2) {
                    continue;
                }
                distances.push({
                    index: i,
                    distance: dist
                });
            }

            distances.sort(function(a, b) {
               if (a.distance > b.distance) {
                    return 1;
               } else if (a.distance < b.distance) {
                    return -1;
               }
               return 0;
            });

            distances.slice(0, 2);

            var relevantAuthors = [];
            for(var i = 0; i < distances.length; i++) {
                relevantAuthors.push(authors[distances[i].index]);
            }

            return relevantAuthors;
        }
    });