angular.module('settingsHolder',[])

.service('userSettings',function() {
        var self = this;
        var settings = {
            fontSize: {number: 16, px: "16px"},
            fontFamily: {name: "Times serif", value: "times, serif" },
            textColor: "#000000",
            backColor: "#FFFFFF"
        };

        this.setFontSize = function(fontS) {
            settings.fontSize = fontS;
        };
        this.setFontFamily = function(fontF) {
            settings.fontFamily = fontF;
        };
        this.setTextColor = function(textC) {
            settings.textColor = textC;
        };
        this.setBackColor = function(backC) {
            settings.backColor = backC;
        };
        this.getSettings = function(){
            self.loadData();
            return settings;
        };

        this.saveData = function() {
            window.localStorage.setItem("Settings",JSON.stringify(settings));
        }

        this.loadData = function() {
            try {
                var data = JSON.parse(window.localStorage.getItem("Settings"));
                if (data !== null && data !== undefined) {
                    settings = data;
                }
            } catch(err) {
                console.log(err);
            }
        }
    });
