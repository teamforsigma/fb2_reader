angular.module('utils.fs.reader', ['utils.fs'])
    .service('appDirs', function ($q) {
        "use strict";
        var dirs = {};

        var getDirInApp = function (name) {
            var deferred = $q.defer();
            if (dirs[name] === undefined) {
                window.resolveLocalFileSystemURL(cordova.file.dataDirectory + name, function (directory) {
                    dirs[name] = directory;
                    deferred.resolve(directory);
                }, deferred.reject);
            } else {
                deferred.resolve(dirs[name]);
            }
            return deferred.promise;
        };

        this.getBooksDir = function () {
            return getDirInApp("");
        };

        this.getCoversDir = function () {
            return getDirInApp("");
        };
    })
    .service('appStorage', function ($q, fileReader, appDirs, fileRemover) {
        "use strict";
        var createAppDirs = function () {

        };
        createAppDirs();

        this.getBook = function (name, charset) {
            return fileReader.readAsText(appDirs.getBooksDir(), name, charset);
        };

        this.getBookCoverSrc = function (book) {
            return appDirs.getCoversDir().then(function (dir) {
                var deferred = $q.defer();
                if (book.coverpageExtension === "") {
                    deferred.resolve("img/book.png");
                } else {
                    var fileName = book.id + '.' + book.coverpageExtension;
                    dir.getFile(fileName, {create: false}, function (file) {
                        file.file(function (file) {
                            var reader = new FileReader();
                            reader.onloadend = function (evt) {
                                deferred.resolve(evt.target.result);
                            };
                            reader.onabort = function () {
                                deferred.reject(new Error("Aborted"));
                            };
                            reader.onerror = deferred.reject;
                            reader.readAsDataURL(file);
                        });
                    }, deferred.reject);
                }
                return deferred.promise;
            })
        };

        this.saveBookCoverpage = function (book, binary) {
            return appDirs.getCoversDir().then(function (dir) {
                var deferred = $q.defer();
                if (book.coverpageExtension === "") {
                    deferred.resolve();
                } else {
                    dir.getFile(book.id + '.' + book.coverpageExtension, {create: true, exclusive: false}, function (fileEntry) {
                        fileEntry.createWriter(function (writer) {
                            writer.onwrite = deferred.resolve;
                            writer.onerror = deferred.reject;
                            writer.onabort = deferred.reject;

                            var imageUrl = "data:image/" + book.coverpageExtension + ";base64," + binary.data;
                            var blob = window.dataURLtoBlob(imageUrl);
                            writer.write(blob);
                            deferred.resolve();
                        }, deferred.reject);
                    }, deferred.reject);
                }
                return deferred.promise;
            });
        };

        this.deleteBook = function (bookName) {
            return appDirs.getBooksDir().then( function (dir) {
                return fileRemover.remove(dir, bookName);
            });
        };

        this.deleteBookCoverpage = function (book) {
            return appDirs.getCoversDir().then(function (dir) {
                return fileRemover.remove(dir, book.id + '.' + book.coverpageExtension);
            });
        }
    })
    .service('externalFileResolver', function ($q, fsService) {
        "use strict";
        this.resolve = function (path) {
            var deferred = $q.defer();
            var fileName = path.substr(path.lastIndexOf('/') + 1);
            fsService.getLocalFS().then(function (fs) {
                var dir = fs.root.toURL();
                if (path.indexOf(dir) === 0) {
                    path = path.substring(dir.length);
                }
                fs.root.getFile(path, null, function (file) {
                    var result = {
                        name: fileName,
                        file: file
                    };
                    deferred.resolve(result);
                }, deferred.reject);
            }, deferred.reject);
            return deferred.promise;
        };
    })
    .service('fileMover', function ($q, appDirs, externalFileResolver) {
        "use strict";

        var move = function (file, directory) {
            var deferred = $q.defer();
            file.copyTo(directory, file.name, function () {
                console.log("File copied as ", file.name);
                deferred.resolve(file.name);
            }, deferred.reject);
            return deferred.promise;
        };

        this.moveToLocalLibrary = function (file) {
            if (typeof file === 'string') {
                return externalFileResolver.resolve(file).then(function (result) {
                    return appDirs.getBooksDir().then(function (directory) {
                        return move(result.file, directory);
                    });
                });
            } else {
                return appDirs.getBooksDir().then(function (directory) {
                    return move(file, directory);
                });
            }
        };
    })
    .service('externalFileReader', function ($q, externalFileResolver) {
        "use strict";

        var readFileAsText = function (file, charset) {
            var deferred = $q.defer();
            var reader = new FileReader();
            reader.onloadend = function (evt) {
                deferred.resolve(evt.target.result);
            };
            reader.onabort = function () {
                deferred.reject(new Error("Aborted"));
            };
            reader.onerror = deferred.reject;
            reader.readAsText(file, charset);
            return deferred.promise;
        };

        this.readAsText = function (file, charset) {
            var deferred = $q.defer();
            if (typeof file === 'string') {
                return externalFileResolver.resolve(file).then(function (result) {
                    file.file.file(function (file) {
                        readFileAsText(result, charset).then(deferred.resolve, deferred.reject);
                    });
                });
            } else {
                file.file(function (file) {
                    readFileAsText(file, charset).then(deferred.resolve, deferred.reject);
                });
            }
            return deferred.promise;
        };
    })
    .service('fileRemover', function ($q) {
        this.remove = function (dir, fileName) {
            var deferred =  $q.defer();
            dir.getFile(fileName, {create : false}, function (fileEntry) {
                fileEntry.remove();
                deferred.resolve();
            }, deferred.resolve);
            return deferred.promise;
        };
    })
    .factory('fileReader', function ($injector) {
        "use strict";
        var fileReaderService = $injector.get('androidFileReader');
        return fileReaderService;
    })
    .service('androidFileReader', function ($q) {
        'use strict';
        this.readAsText = function (dir, path, charset) {
            console.log("File: " + path);
            var deferred = $q.defer();

            dir.then(function (directory) {
                directory.getFile(path, null, function (file) {

                    file.file(function (file) {
                        var reader = new FileReader();
                        reader.onloadend = function (evt) {
                            deferred.resolve(evt.target.result);
                        };
                        reader.onabort = function () {
                            deferred.reject(new Error("Aborted"));
                        };
                        reader.onerror = deferred.reject;
                        reader.readAsText(file, charset);
                    });
                }, deferred.reject);
            }, deferred.reject);
            return deferred.promise;
        };
    });