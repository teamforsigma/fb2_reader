/**
 * Created by Dmitryy on 31-Oct-14.
 */
angular.module('utils.fs.writer', ['utils.fs', 'utils.fs.reader'])

    .service('androidFileWriter', function ($q, appDirs) {

        this.writeStringToFile = function (bookText, fileName) {
            var defered = $q.defer();
            var bookDir = appDirs.getBooksDir().then(function (dir) {
                dir.getFile(fileName, {create: true, exclusive: false}, function (fileEntry) {
                        //success
                        console.log('root getFile succeeded');
                        var filePath = fileEntry.fullPath;
                        fileEntry.createWriter(function (writer) {
                                //suc
                                console.log('File entry succeeded');
                                writer.onwriteend = function (event) {
                                    console.log('WriteFile succeeded');
                                    defered.resolve(filePath);
                                };
                                writer.onerror = function (er) {
                                    console.log('WriteFile error');
                                    defered.reject(er);
                                };
                                writer.onabort = function (msg) {
                                    console.log('WriteFile aborted for some reasons');
                                    defered.reject(msg);
                                };

                                writer.write(bookText);
                            },
                            function (er) {
                                //er
                                console.log('File entry error');
                            });

                    },
                    function (er) {
                        //error
                        console.log('root getFile error');
                    });
            });
        };
    }
);