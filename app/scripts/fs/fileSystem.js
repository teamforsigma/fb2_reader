angular.module('utils.fs', [])
    .service('fsService', function(fs) {
        "use strict";
        this.getLocalFS = function() {
            return fs.get(LocalFileSystem.PERSISTENT);
        };
        this.getTempFS = function() {
            return fs.get(LocalFileSystem.TEMPORARY);
        };
    })
    .factory('fs', function ($q) {
        'use strict';
        var fileSystems = {};
        this.get = function (fsType) {
            var deferred = $q.defer();
            if (fileSystems[fsType] === undefined) {
                var func = window.webkitRequestFileSystem || window.requestFileSystem;
                func(window.PERSISTENT, 1024 * 1024, function (fs) {
                    fileSystems[fsType] = fs;
                    console.log("FS request success");
                    deferred.resolve(fs);
                }, function (e) {
                    console.log("FS request failed");
                    deferred.reject(e);
                });
            } else {
                deferred.resolve(fileSystems[fsType]);
            }
            return deferred.promise;
        };
        return this;
    });