/**
 * Created by Dmitryy on 09-Oct-14.
 */
angular.module('parameters', [])

    //redo with proper scaling
    .service('windowPadding', function () {
        this.getTopPadding = function () {
            return 20;
        };
        this.getLeftPadding = function () {
            return 20;
        };

    })
    .service('windowSize', function () {
        this.getWindowHeight = function () {
            var windowHeight = 0;
            if (typeof (window.innerHeight) == 'number') {
                windowHeight = window.innerHeight;
            } else if (document.documentElement && document.documentElement.clientHeight) {
                windowHeight = document.documentElement.clientHeight;
            } else if (document.body && document.body.clientHeight) {
                windowHeight = document.body.clientHeight;
            }

            //THIS
            //windowHeight = window.screen.height;
            //windowHeight =window.screen.availHeight;
            windowHeight = $(window).height();
            return windowHeight;
        };
        this.getWindowWidth = function () {
            var windowWidth = 0;
            if (typeof (window.innerWidth) == 'number') {
                windowWidth = window.innerWidth;
            } else if (document.documentElement && document.documentElement.clientWidth) {
                windowWidth = document.documentElement.clientWidth;
            } else if (document.body && document.body.clientWidth) {
                windowWidth = document.body.clientWidth;
            }

            return windowWidth;
        };
    })
    .factory('windowParams', function (windowPadding, windowSize) {
        var params = {
            topPadding: windowPadding.getTopPadding(),
            leftPadding: windowPadding.getLeftPadding(),
            width: windowSize.getWindowWidth(),
            height: windowSize.getWindowHeight()
        };
        return params;
    })
;

