/**
 * Created by Vlada on 27.10.2014.
 */

angular.module('popup.gotoNext', ['ionic'])
    .factory('gotoNextPopup', function ($ionicPopup, $state) {
        "use strict";
        return {
            show: function ($scope) {
                var pageNumber = { value: 1 };
                $scope.pageNumber = pageNumber;
                $scope.showError = false;

                var nPopup = $ionicPopup.show({
                    subTitle: '<h3>Go to page:</h3>',
                    scope: $scope,
                    templateUrl: 'templates/popup/popup-goto-page.html',
                    buttons: [
                        { text: 'Cancel' },
                        {
                            text: '<h4>Go</h4>',
                            type: 'button button-outline button-positive',
                            onTap: function (e) {
                                var pagesNum = $scope.pages.length;
                                var pageIndex = pageNumber.value - 1;
                                if (pageIndex < pagesNum && pageIndex >= 0) {
                                    $scope.showError = false;
                                    $state.go('menu.book', { startIndex: pageIndex, caller: 'gotoPage'});
                                } else {
                                    e.preventDefault();
                                    $scope.showError = true;
                                }
                            }
                        }
                    ]
                });
            }
        };
    });