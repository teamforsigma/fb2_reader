angular.module('popup.note', ['ionic', 'text.selection', 'notesManager'])
    .factory('noteAddPopup', function ($ionicPopup, selectionHandler) {
        "use strict";
        return {
            show: function ($scope) {
                var selectionInfo = selectionHandler.getSelectionInfo();
                var note = {
                    name: "",
                    comment: selectionInfo.selectedText,
                    paragraphNum: selectionInfo.paragraphNum,
                    firstWordNum: selectionInfo.firstWordNum
                };

                $scope.note = note;

                var popup = $ionicPopup.show({
                    templateUrl: 'templates/popup/popup-note.html',
                    subTitle: '<h3>Add note</h3>',
                    scope: $scope,
                    buttons: [
                        { text: 'Cancel' },
                        {
                            text: '<b>Save</b>',
                            type: 'button-positive',
                            onTap: function() {
                                $scope.saveNote(note);
                            }
                        }
                    ]
                });

            }
        };
    });
