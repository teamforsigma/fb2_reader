/**
 * Created by Constantine on 29.10.2014.
 */

angular.module('popup.bookOptions', ['ionic', 'popup.deleteBookConfirm', 'popup.rateBook'])
    .factory('bookOptionsPopup', function ($ionicPopup, deleteBookConfirmPopup, rateBookPopup) {
        "use strict";
        return {
            show: function ($scope, book) {

                $scope.ClosePopUp = function() {
                    oPopup.close();
                };

                $scope.viewBookClick = function() {
                    $scope.ClosePopUp();
                    $scope.openBookFromLibrary(book);
                };

                $scope.rateBookClick = function() {
                    rateBookPopup.show($scope, book);
                };

                $scope.deleteBookClick = function() {
                    deleteBookConfirmPopup.show($scope, book);
                };

                var oPopup = $ionicPopup.show({
                    templateUrl: 'templates/popup/popup-book-options.html',
                    title: 'Book options',
                    subTitle: book.bookName,
                    scope: $scope,
                    buttons: [
                        { text: 'Cancel' }
                    ]
                });

            }
        };
    });
