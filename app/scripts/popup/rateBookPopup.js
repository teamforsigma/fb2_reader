/**
 * Created by Constantine on 29.10.2014.
 */

angular.module('popup.rateBook', ['ionic', 'twitterParams', 'facebookParams'])
    .factory('rateBookPopup', function ($ionicPopup, $state, twitterParamsHolder, facebookParamsHolder) {
        "use strict";
        return {
            show: function ($scope, book) {

                $scope.currentRate = book.rating;

                $scope.setCurrentRate = function(rate) {
                    $scope.currentRate = rate;
                    $scope.updateBookRate(book.id, $scope.currentRate);
                };

                var popup = $ionicPopup.show({
                    templateUrl: 'templates/popup/popup-book-rate.html',
                    title: 'Rate book',
                    subTitle: book.bookName,
                    scope: $scope,
                    buttons: [
                        {
                            text: '',
                            type: 'icon ion-social-facebook button-positive',
                            onTap: function() {
                                $scope.ClosePopUp();
                                facebookParamsHolder.setBookRate(book, $scope.currentRate);
                                $state.go('menu.facebook');
                            }
                        },
                        {
                            text: '',
                            type: 'icon ion-social-twitter button-calm',
                            onTap: function() {
                                $scope.ClosePopUp();
                                twitterParamsHolder.setLastMessageTextRate(book, $scope.currentRate);
                                $state.go('menu.twitter');
                            }
                        },
                        {
                            text: 'OK',
                            onTap: function() {
                                $scope.ClosePopUp();
                            }
                        }
                    ]
                });

            }
        };
    });
