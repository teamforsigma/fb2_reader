
angular.module('popup.fitAuthor', ['ionic'])
    .factory('fitAuthorPopup', function ($ionicPopup, $rootScope, $q, $ionicLoading) {
        "use strict";
        return {
            show: function ( originalAuthor, relevantAuthors) {
                //var $scope = $rootScope.new(true);
                var $scope = $rootScope;
                var deferred = $q.defer();
                $ionicLoading.hide();

                var save = function () {
                    deferred.resolve($scope.relevantAuthors[$scope.selectedIndex].authorId); //return selected item id
                }
                $scope.save = save;
                $scope.selectedIndex = -1;
                $scope.originalAuthor = originalAuthor;
                $scope.relevantAuthors = relevantAuthors;
                $scope.showError = false;

                $scope.select = function(index) {
                    $scope.selectedIndex = index;
                    $('.div-author.selected').removeClass('selected'); //unselect
                    $('.div-author').addClass('selected'); //select at index
                }

                var popup = $ionicPopup.show({
                    templateUrl: 'templates/popup/popup-fit-authors.html',
                    subTitle: '<h3><p>There are similiar authors in your library.</p><p>Maybe you want to attach this book to one of them?</p></h3>',
                    scope: $scope,
                    buttons: [
                        {
                            text: 'Cancel' ,
                            onTap: function() {
                                deferred.reject();
                                //$scope.destroy();
                            }
                        },
                        {
                            text: '<b>Save</b>',
                            type: 'button-positive',
                            onTap: function(e) {
                                if ($scope.selectedIndex === -1) {
                                    e.preventDefault();
                                    $scope.showError = true;
                                } else {
                                    $scope.save();
                                    //$scope.destroy(); //not sure if needed
                                }
                            }
                        }
                    ]
                });

                return deferred.promise;
            }
        };
    });