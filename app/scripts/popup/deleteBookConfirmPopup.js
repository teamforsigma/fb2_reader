/**
 * Created by Constantine on 29.10.2014.
 */

angular.module('popup.deleteBookConfirm', ['ionic'])
    .factory('deleteBookConfirmPopup', function ($ionicPopup) {
        "use strict";
        return {
            show: function ($scope, book) {
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Deleting book',
                    template: 'Are you sure you want to delete "' +  book.bookName + '" book?'
                });
                confirmPopup.then(function(res) {
                    if(res) {
                        //U sure :D
                        $scope.ClosePopUp();
                        $scope.deleteBook(book);
                    } else {
                        $scope.ClosePopUp();
                    }
                });
            }
        };
    });