/**
 * Created by Vlada on 19.10.2014.
 */
angular.module('popup.addBookmark', ['ionic'])
    .factory('bookmarkAddPopup', function ($ionicPopup ) {
        "use strict";
        return {
            show: function ($scope) {
                var bm = { name: ''};
                $scope.bm = bm;
                var bmPopup = $ionicPopup.show({
                    templateUrl: 'templates/popup/popup-add-bookmark.html',
                    subTitle: '<h3>Add bookmark</h3>',
                    scope: $scope,
                    buttons: [
                        { text: 'Cancel' },
                        {
                            text: '<b>Save</b>',
                            type: 'button-positive',
                            onTap: function() { $scope.saveBookmark(bm.name) }
                        }
                    ]
                });
            }
        };
    });