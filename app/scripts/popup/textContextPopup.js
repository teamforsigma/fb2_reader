angular.module('popup.context', ['ionic', 'text.selection', 'popup.note', 'twitterParams', 'facebookParams'])

    .factory('textContextPopup', function (selectionHandler, noteAddPopup, $state, twitterParamsHolder, facebookParamsHolder) {

        "use strict";
        return {
            init: function ($scope) {
                $scope.showPopup = false;

                $scope.onCopyClick = function () {

                };

                $scope.onNoteClick = function () {
                    $scope.showPopup = false;
                    selectionHandler.hide();
                    noteAddPopup.show($scope);
                };

                $scope.onShareClick = function () {
                    twitterParamsHolder.setLastMessageTextQuote();
                    $scope.setFullScreenMode(false);
                    $state.go('menu.twitter');
                };

                $scope.onShareFacebookClick = function () {
                    facebookParamsHolder.setQuote();
                    $scope.setFullScreenMode(false);
                    $state.go('menu.facebook');
                };

                $scope.popupSelection = $("#popupSelection");

                var popupHeight = 45; // FIXME: hardcode

                $scope.$on('selectionStart', function (event) {
                    $scope.showPopup = true;
                });
                $scope.$on('selectionEnd', function (event) {
                    $scope.showPopup = false;
                });
                $scope.$on('selectionChanged', function (event, selection) {
                    if (popupHeight === 0) {
                        popupHeight = $scope.popupSelection.outerHeight();
                    }
                    var startCoords = selection.start.lastCoords;
                    var endCoords = selection.end.lastCoords;
                    var newPopupPos = {top: 0};
                    if (startCoords.top > popupHeight * 1.5) {
                        // It's possible to show popup up the selection
                        newPopupPos.top = startCoords.top - 1.25 * popupHeight;
                    } else if (endCoords.top - startCoords.top > 3 * popupHeight) {
                        // It's possible to show popup between selection
                        newPopupPos.top = startCoords.top + (endCoords.top - startCoords.top) / 2;
                    } else {
                        // Show under selection
                        newPopupPos.top = endCoords.top + popupHeight * 1.25;
                    }
                    $scope.popupSelection.css(newPopupPos);
                });
            }
        };
    });
