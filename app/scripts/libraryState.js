angular.module('libraryDataHolder', ['db.functions', 'db.manager', 'popup.fitAuthor','fitAuthorService'])

    .service('libraryData', function ($q, dbCRUD, db, appStorage, fitAuthorPopup, fitAuthorService) {
        var self = this;
        var authorsList = [];
        var booksList = [];
        var genresList = [];
        var authorOfLastAddedBook = {};

        var deferred = $q.defer();
        this.ready = deferred.promise;

        var getBookCoverpage = function (book) {
            return appStorage.getBookCoverSrc(book).then(function (src) {
                book.coverpage = src;
            });
        };

        var init = function () {
            console.log("in init");
            var deferred = $q.defer();
            dbCRUD.getAuthors().then(function (aList) {
                authorsList = aList;
                console.log("in autorsList " + aList.length);
                dbCRUD.getBooks().then(function (bList) {
                    booksList = bList;
                    $.each(booksList, function (index, book) {
                       getBookCoverpage(book);
                    });
                    console.log("in booksList " + bList.length);
                    dbCRUD.getGenres().then(function (gList) {
                        genresList = gList;
                        console.log("in genresList " + gList.length);
                        deferred.resolve();
                    }, deferred.reject);
                }, deferred.reject);
            }, deferred.reject);
            return deferred.promise;

        };

        db.whenInitialize.then(init).then(function () {
            deferred.resolve()
        });

        this.updateBookCoverpage = function (book) {
            return getBookCoverpage(book);
        };

        var getBooksShortInfo = function (filter) {
            var booksShortInfo = [];
            for (var i = 0; i < booksList.length; i++) {
                if (filter(booksList[i])) {
                    var currentBookAuthor = getAuthorByID(booksList[i].authorId);
                    var currentBookName = booksList[i].title.toString();
                    var currentBookGenre = booksList[i].genre.toString();
                    var bookAuthorName = currentBookAuthor.firstName.toString() + " " + currentBookAuthor.middleName.toString() + " " + currentBookAuthor.lastName.toString();
                    var currentBookShortInfo = {
                        id: booksList[i].id,
                        bookName: currentBookName,
                        bookGenre: currentBookGenre,
                        authorName: bookAuthorName,
                        authorId: booksList[i].authorId,
                        rating: booksList[i].rate,
                        lastOpenDate: booksList[i].lastOpenDate,
                        coverpage: booksList[i].coverpage,
                        coverpageExtension: booksList[i].coverpageExtension
                    };
                    booksShortInfo.push(currentBookShortInfo);
                }
            }
            return booksShortInfo;
        };

        this.getBooksShortInfo = function () {
            return getBooksShortInfo(function() {return true;});
        };

        this.getBooksRecentInfo = function() {
            return getBooksShortInfo(function() {return true;});
        };

        var getAuthorByID = function (authorID) {
            var authorIndex = -1;
            for (var i = 0; i < authorsList.length; i++) {
                if (authorsList[i].authorId === authorID)
                    authorIndex = i;
            }
            return authorsList[authorIndex];
        };

        this.getAuthorsShortInfo = function () {
            var authorsShortInfo = [];
            for (var i = 0; i < authorsList.length; i++) {
                var currentAuthorName = authorsList[i].firstName + " " + authorsList[i].middleName + " " + authorsList[i].lastName;
                var currentAuthorShortInfo = {
                    id: authorsList[i].authorID,
                    name: currentAuthorName
                };
                authorsShortInfo.push(currentAuthorShortInfo);
            }
            return authorsShortInfo;
        };

        this.getBooksShortInfoByAuthorID = function (authorID) {
            return getBooksShortInfo(function (book) {
                return book.authorId == authorID;
            });
        };

        this.getBooksShortInfoByGenre = function (genre) {
            return getBooksShortInfo(function (book) {
                return book.genre == genre;
            });
        };

        this.openNewBook = function (book) {
            var deferred = $q.defer();
            var author = {};
            author.authorId;
            author.firstName = book.description.titleInfo.author[0].firstName;
            author.middleName = book.description.titleInfo.author[0].middleName;
            author.lastName = book.description.titleInfo.author[0].lastName;
            author.nickname = book.description.titleInfo.author[0].nickname;
            checkAuthor(author).then(function (isNew) {
                authorOfLastAddedBook = author;
                var bookMeta = {};
                bookMeta.id;
                bookMeta.ISBN = book.description.publishInfo.isbn;
                bookMeta.authorId = author.authorId;
                bookMeta.genre = book.description.titleInfo.genre[0];
                bookMeta.title = book.description.titleInfo.bookTitle;
                bookMeta.releaseDate = book.description.titleInfo.date;
                bookMeta.annotation = book.description.titleInfo.annotation;
                bookMeta.encoding = book.encoding;
                bookMeta.lastReadPage = 0;
                bookMeta.fileName = book.fileName;
                bookMeta.rate = 0;
                bookMeta.lastOpenDate = new Date();
                bookMeta.coverpageExtension = book.coverpageExtension;
                checkBook(bookMeta).then(function () {
                    if (isNew) {
                        var listRelevant = fitAuthorService.getRelevantAuthors(author, authorsList);
                        if (listRelevant.length > 0)
                            fitAuthorPopup.show(author, listRelevant).then(
                                function(selectedAuthorId) {
                                    mergeAuthors(author.authorId, selectedAuthorId);
                                },
                                function() {
                                    console.log('rejected from fit author popup');
                                }
                            );
                    }
                    deferred.resolve(bookMeta);
                }, function (err) {
                    deferred.reject(err);
                });
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        var checkAuthor = function (author) {
            var deferred = $q.defer();
            var isNew = true;
            for (var i = 0; i < authorsList.length; i++) {
                if (authorsList[i].firstName === author.firstName && authorsList[i].middleName === author.middleName &&
                    authorsList[i].nickname === author.nickname && authorsList[i].lastName === author.lastName) {
                    isNew = false;
                    author.authorId = authorsList[i].authorId;
                }
            }
            if (isNew) {
                dbCRUD.addAuthor(author).then(function (id) {
                    author.authorId = id;
                    authorsList.push(author);
                    deferred.resolve(isNew);
                }, function (err) {
                    deferred.reject(err);
                });
            } else {
                deferred.resolve(isNew);
            }
            return deferred.promise;
        };

        var checkBook = function (book) {
            var deferred = $q.defer();
            var isNew = true;
            var bookIndex = -1;
            for (var i = 0; i < booksList.length; i++) {
                if (booksList[i].authorId === book.authorId && booksList[i].genre === book.genre && booksList[i].title === book.title) {
                    isNew = false;
                    book.id = booksList[i].id;
                    bookIndex = i;
                }
            }
            if (isNew) {
                book.lastReadPage = 0;
                dbCRUD.addBook(book).then(function (id) {
                    book.id = id;
                    booksList.push(book);
                    deferred.resolve();
                }, function (err) {
                    deferred.reject(err);
                });
            } else {
                booksList[bookIndex].lastOpenDate = Date();
                book.id = booksList[bookIndex].id;
                book.rate = booksList[bookIndex].rate;
                book.lastReadPage = booksList[bookIndex].lastReadPage;
                console.log("checkBook -> id "+ booksList[bookIndex].id + " lastopendate " + booksList[bookIndex].lastOpenDate);
                dbCRUD.updateBook(booksList[bookIndex]).then(deferred.resolve, function (err) {
                    console.log(err.message);
                    deferred.reject(err);
                });
            }
            return deferred.promise;
        };

        this.updateLastReadPage = function (bookId, pageIndex) {
            var book = self.getBookById(bookId);
            book.lastReadPage = pageIndex;
            return dbCRUD.updateBook(book);
        };

        this.deleteBookByID = function (bookID) {
            var deleteBook = function (bookMeta) {
                booksList.splice(bookIndex, 1);
                return dbCRUD.deleteBook(bookID).then(function() {
                    return appStorage.deleteBook(bookMeta.fileName);
                }).then(function () {
                    return appStorage.deleteBookCoverpage(bookMeta.id);
                });
            };

            var bookIndex = -1;
            for (var i = 0; i < booksList.length; i++) {
                if (bookID === booksList[i].id)
                    bookIndex = i;
            }
            if (bookIndex === -1)
                return;
            if (countAuthorsBooks(booksList[bookIndex].authorId) === 1) {
                return deleteAuthor(booksList[bookIndex].authorId).then(function () {
                    return deleteBook(booksList[bookIndex]);
                });
            } else {
                return deleteBook(booksList[bookIndex]);
            }
        };

        var countAuthorsBooks = function (authorID) {
            var count = 0;
            for (var i = 0; i < booksList.length; i++) {
                if (booksList[i].authorId === authorID)
                    count++;
            }
            return count;
        };

        var deleteAuthor = function (authorID) {
            var authorIndex = -1;
            for (var i = 0; i < authorsList.length; i++) {
                if (authorsList[i].authorId === authorID)
                    authorIndex = i;
            }
            if (authorIndex === -1) {
                return;
            }
            authorsList.splice(authorIndex, 1);
            return dbCRUD.deleteAuthor(authorID);
        };

        this.getLastOpenedBook = function () {
            console.log("books list: " + booksList + " length: " + booksList.length);
            if (booksList === undefined || booksList.length === 0) {
                return null;
            }
            var lastOpenDateIndex = 0;
            for (var i = 1; i < booksList.length; i++) {
                if (booksList[i].lastOpenDate > booksList[lastOpenDateIndex].lastOpenDate) {
                    lastOpenDateIndex = i;
                }
            }
            return booksList[lastOpenDateIndex];
        };

        this.getBookById = function (bookID) {
            var gettedBook = {};
            for (var i = 0; i < booksList.length; i++) {
                if (bookID === booksList[i].id) {
                    gettedBook = booksList[i];
                    break;
                }
            }
            return gettedBook;
        };

        this.updateBookRate = function(bookID, newRate) {
            var book = this.getBookById(bookID);
            book.rate = newRate;
            return dbCRUD.updateBook(book);
        }

        this.getAuthors = function() {
            return authorsList;
        }

        this.getAuthorOfLastAddedBook = function() {
            return authorOfLastAddedBook;
        }

        this.mergeAuthors = function(idSubject, idTarget) {
            for (var i = 0; i < booksList.length; i++) {
                if (booksList[i].authorId === idSubject) {
                    booksList[i].authorId = idTarget;
                }
            }
            deleteAuthor(idSubject);
        }
        var mergeAuthors = this.mergeAuthors;
    });
