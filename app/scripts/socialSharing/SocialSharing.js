angular.module('twitterShare', ['ionic', 'twitterLib', 'twitterParams'])

  .controller('TwitterPageCtrl', function ($scope, TwitterLib, twitterParamsHolder) {

    $scope.tweetText;
    $scope.name = "Login please";
    var networkState;

    var produceTweet = function () {
      $scope.tweetText = " #KlappaReader";
      var selectedText = twitterParamsHolder.getLastMessageText();
      $scope.tweetText = selectedText + $scope.tweetText;
    }
    produceTweet();
    /**
     *
     */
    $scope.doLogin = function () {
      networkState = navigator.connection.type;
      if (networkState === 'none') {
        alert('No network connection');
      } else {
        TwitterLib.init().then(function (_data) {
          console.log(JSON.stringify(_data));
          $scope.name = _data.name;
        }, function error(_error) {
          console.log(JSON.stringify(_error));
        });
      }
    };

    $scope.doLogin();
    /**
     *
     */
    $scope.doLogout = function () {
      TwitterLib.logOut();
      $scope.name = "Login please";
    };
    /**
     *
     */
    $scope.doStatus = function () {
      var options = {
        url: "https://api.twitter.com/1.1/statuses/user_timeline.json",
        data: {
          'screen_name': "aaronksaunders",
          'count': "25"
        }
      };
      TwitterLib.apiGetCall(options).then(function (_data) {
        console.log("doStatus success");
        $scope.items = _data;

      }, function (_error) {
        console.log("doStatus error" + JSON.stringify(_error));
      });
    };
    /**
     *
     */
    $scope.doTweet = function () {
      TwitterLib.tweet($scope.tweetText).then(function (_data) {
        console.log("tweet success");
        alert("tweeted :D");
      }, function (_error) {
        console.log("tweet error" + JSON.stringify(_error));
      });
    };
  })
  .run(function ($ionicPlatform, TwitterLib) {
    $ionicPlatform.ready(function () {
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }

    });
  });

//
// HERE ARE THE CONFIGURATION SETTINGS FOR OAUTH
// REPLACE  THESE VALUES WITH YOUR KEYS FROM TWITTER
//
angular.module('twitterShare').constant('myAppConfig', {
  oauthSettings: {
    consumerKey: 'rjcbY8wjWTNDlf81d2qQhMASE',
    consumerSecret: 'MRjfHPIhMtLFfj15gRkwXh53jwkQVjIU3DAK7SEojbuVvwQ5Ch',
    requestTokenUrl: 'https://api.twitter.com/oauth/request_token',
    authorizationUrl: "https://api.twitter.com/oauth/authorize",
    accessTokenUrl: "https://api.twitter.com/oauth/access_token",
    callbackUrl: "https://atat.auth0.com/mobile"
  }
});
