angular.module('facebookParams', ['text.selection'])

    .service('facebookParamsHolder', function (selectionHandler) {
        var postText = "";

        this.getPostText = function () {
            return postText;
        };

        this.setQuote = function () {
            postText = selectionHandler.getSelectionInfo().selectedText;
        };

        this.setBookRate = function (book, rate) {
            var bookFullName = book.authorName + " \"" + book.bookName + "\"";
            postText = "Rated " + bookFullName + " for " + rate + " out of 5";
        };
    });
