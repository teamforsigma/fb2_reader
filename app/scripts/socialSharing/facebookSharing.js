angular.module('facebookSharingModule', ['ionic', 'text.selection', 'facebookParams'])

  .controller('FacebookPageCtrl', function ($scope, facebookParamsHolder, $state) {
    $scope.name = "Login please";
    $scope.postText = facebookParamsHolder.getPostText() + '\n' + " #KlappaReader";
    var networkState;
    var states = {};

    $scope.login = function () {
      networkState = navigator.connection.type;
      if (networkState === 'none') {
        alert("WiFi is off. Login denied");
      }
      else {
        openFB.login(
          function (response) {
            if (response.status === 'connected') {
              openFB.api({
                path: '/me',
                params: {fields: 'id,name'},
                success: function (user) {
                  $scope.$apply(function () {
                    $scope.name = user.name;
                  });
                },
                error: function (error) {
                  console.log("No facebook user");
                  $scope.name = "Login please";
                }
              });
              //alert('Facebook login succeeded, got access token: ' + response.authResponse.token);
            } else {
              alert('Facebook login failed: ' + response.error);
            }
          }, {scope: 'email,read_stream,publish_stream,publish_actions'});
      }
    };
    $scope.login();

    $scope.getInfo = function () {
      openFB.api({
        path: '/me',
        success: function (data) {
          console.log(JSON.stringify(data));
          //$scope.name = data.name;
          document.getElementById("userPic").src = 'http://graph.facebook.com/' + data.id + '/picture?type=small';
        },
        error: errorHandler
      });
    };

    $scope.share = function () {
      openFB.api({
        method: 'POST',
        path: '/me/feed', /*/me/feed*/
        params: {
          message: $scope.postText /*document.getElementById('Message').value*/ || 'Testing Facebook APIs'
        },
        success: function () {
          alert('the item was posted on Facebook');
        }
      });
    };

    $scope.revoke = function () {
      openFB.revokePermissions(
        function () {
          //alert('Permissions revoked');
        });
    };

    $scope.logout = function () {
      openFB.logout(
        function () {
          alert('Logout successful');
          $state.go('menu.book');
        });
    };

    $scope.post = function (event) {
      openFB.api({
        method: 'POST',
        path: '/me/feed',
        params: {
          message: $scope.postText
        },
        success: function () {
          //alert('The session was shared on Facebook');
        },
        error: function () {
          alert('An error occurred while sharing this session on Facebook');
        }
      });
    };

    $scope.errorHandler = function (error) {
      alert(error.message);
    }
  });
