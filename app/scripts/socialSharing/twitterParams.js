angular.module('twitterParams', ['text.selection'])

  .service('twitterParamsHolder',function(selectionHandler) {
    var lastMessageText = "";

    this.getLastMessageText = function() {
      return lastMessageText;
    }

    this.setLastMessageTextQuote = function() {
      var quote = selectionHandler.getSelectionInfo().selectedText;
      if (quote.length > 126) {
        lastMessageText = quote.substring(0,122);
        lastMessageText += "...";
      } else {
        lastMessageText = quote;
      }
    }

    this.setLastMessageTextRate = function(book, rate) {
      var bookFullName = book.authorName + " \"" + book.bookName + "\"";
      if (bookFullName.length > 105) {
        bookFullName = bookFullName.substring(0, 100);
        bookFullName += "...\"";
      }
      lastMessageText = "Rated " + bookFullName + " for " + rate + " out of 5";
    }

  })
