/**
 * Created by Dmitryy on 14-Oct-14.
 */
angular.module('bookmarkManager', ['db.functions', 'currentBookTracker'])

    .service('bookmarks', function (dbCRUD, currentBook, $q) {
        //returns promises straight from db
        this.saveBookmark = function (bmPosition, bmName) {
            var deferred = $q.defer();
            var book = currentBook.getBook();
            var bookmarkObject = {
                bookId: book.id,
                name: bmName,
                position: bmPosition
            };
            dbCRUD.addBookmark(bookmarkObject).then(
                function (id) {
                    bookmarkObject.bookmarkId = id;
                    deferred.resolve(bookmarkObject);
                },
                deferred.reject);
            return deferred.promise;
        };

        /* gets bookmarks for current book */
        this.getBookmarks = function () {
            var book = currentBook.getBook();
            return dbCRUD.getBookmarks(book.id);
        };

        this.deleteBookmark = function (bookmark) {
            return dbCRUD.deleteBookmark(bookmark.id);
        };

        this.findBookmark = function (bookmark) {
            var pageIndex = 0;
            var id = bookmark.position;
            var pages = currentBook.getPages();
            for (var i = 0; i < pages.length; i++) {
                if (pages[i].startId <= id && pages[i].endId >= id) {
                    pageIndex = i;
                    break;
                }
            }
            //var first = 0;
            //var last = pages.length;
            //binary search not tested
//            while (first < last) {
//                var mid = Math.floor((first + last) / 2);
//                if (pages[mid].startId == id) {
//                    pageIndex = mid;
//                    break;
//                } else if (id < pages[mid].startId) {
//                    last = mid - 1;
//                } else {
//                    first = mid + 1;
//                }
//            }
            return pageIndex;
        };

        this.findNextBookmark = function (currentPageIndex, bookmarksList) {
            /****uncomment if list should be extracted here****/
            //var bookmarksList = [];
            //bookmarksList = getBookmarks();
            bookmarksList.sort(function (a, b) {
                return (a.position - b.position);
            });
            var pages = currentBook.getPages();
            var curPageLastTagId = parseInt($(pages[currentPageIndex]).last().attr('id').substring(7));
            var nextBookmark = $.grep(bookmarksList, function (bookmark) {
                return bookmark.position > curPageLastTagId;
            });
            return nextBookmark;
        };
    })
;