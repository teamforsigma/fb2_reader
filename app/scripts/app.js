// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'


angular.module('starter', ['ionic', 'sideMenu.controllers', 'bookModule', 'libraryModule', 'recentModule',
  'settingsModule', 'bookmarksModule', 'notesModule', 'fileExplorerModule', 'twitterShare', 'facebookSharingModule', 'intentModule'])

  .run(function ($ionicPlatform, intentService, $state) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
    });

        $ionicPlatform.onHardwareBackButton(function(e) {
            if ($state.current.name === 'menu.notesBookmarksTabs.notes'
                || $state.current.name === 'menu.notesBookmarksTabs.bookmarks') {
                console.log('caught back button event (onHardwareBackButton)');
                //e.preventDefault(); //does nothing
                $state.go('menu.book');
            }
        })

//        $ionicPlatform.registerBackButtonAction(function(e) {
//            if ($state.current.name === 'menu.notesBookmarksTabs.notes'
//                || $state.current.name === 'menu.notesBookmarksTabs.bookmarks') {
//                console.log('caught back button event (registerBackButtonAction)');
//                e.preventDefault();
//                //goto book?
//            }
//        });
  })

  .config(function ($stateProvider, $urlRouterProvider) {
    openFB.init({appId: '349287805246445'});

    $stateProvider

      .state('menu', {
        url: "/menu",
        abstract: true,
        templateUrl: "templates/side-menu.html"
      })

      .state('menu.book', {
        url: '/book/:caller;:startIndex',
        views: {
          'menuContent': {
            templateUrl: 'templates/page-book.html',
            controller: 'BookController',
            params: ['startIndex', 'caller']
          }
        }
      })

      .state('menu.libraryTabs', {
        url: '/libraryTabs',
        abstract: true,
        views: {
          'menuContent': {
            templateUrl: 'templates/page-library.html',
            controller: 'LibraryController'
          }
        }
      })

      .state('menu.libraryTabs.title', {
        url: '/title/:isAuthor&:text',
        views: {
          'title-tab': {
            templateUrl: 'templates/libraryTabs/tab-title.html',
            controller: 'TitleTabController'
          }
        }
      })

      .state('menu.libraryTabs.author', {
        url: '/author/',
        views: {
          'author-tab': {
            templateUrl: 'templates/libraryTabs/tab-author.html',
            controller: 'AuthorTabController'
          }
        }
      })

      .state('menu.libraryTabs.genre', {
        url: '/genre',
        views: {
          'genre-tab': {
            templateUrl: 'templates/libraryTabs/tab-genre.html',
            controller: 'GenreTabController'
          }
        }
      })

      .state('menu.recent', {
        url: '/recent',
        views: {
          'menuContent': {
            templateUrl: 'templates/page-recent.html',
            controller: 'RecentController'
          }
        }
      })

      .state('menu.setting', {
        url: '/setting',
        views: {
          'menuContent': {
            templateUrl: 'templates/page-settings.html',
            controller: 'SettingsController'
          }
        }
      })

      .state('menu.notesBookmarksTabs', {
        url: '/NBTabs',
        abstract: true,
        views: {
          'menuContent': {
            templateUrl: 'templates/page-notes-bookmarks.html'
          }
        }
      })

      .state('menu.notesBookmarksTabs.notes', {
        url: '',
        views: {
          'notes-tab': {
            templateUrl: 'templates/page-list-notes.html',
            controller: 'NotesController'
          }
        }
      })

      .state('menu.notesBookmarksTabs.bookmarks', {
        url: '/bookmarks',
        views: {
          'bookmarks-tab': {
            templateUrl: 'templates/page-list-bookmarks.html',
            controller: 'BookmarksController'
          }
        }
      })

      .state('menu.fileExplorer', {
        url: '/fileExplorer/',
        views: {
          'menuContent': {
            templateUrl: 'templates/page-file-explorer.html',
            controller: 'FileExplorerController'
          }
        }
      })

      .state('menu.twitter', {
        url: '/twitter',
        views: {
          'menuContent': {
            templateUrl: 'templates/twitter.html',
            controller: 'TwitterPageCtrl'
          }
        }
      })

      .state('menu.facebook', {
        url: '/facebook',
        views: {
          'menuContent': {
            templateUrl: 'templates/facebook.html',
            controller: 'FacebookPageCtrl'
          }
        }
      })

    $urlRouterProvider.otherwise('/menu/book/menu;0');//has to be currentBook.lastReadPage, but not sure if it is saved properly
  })
