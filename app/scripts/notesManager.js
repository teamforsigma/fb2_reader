/**
 * Created by Dmitryy on 16-Oct-14.
 */
angular.module('notesManager', ['db.functions', 'currentBookTracker'])

    .service('notes', function (dbCRUD, currentBook, $q) {
        this.saveNote = function (noteObj) {
            var deferred = $q.defer();
            var curBook = currentBook.getBook();
            var note = {
                bookId: curBook.id,
                name: noteObj.name,
                comment: noteObj.comment,
                paragraphNum: noteObj.paragraphNum,
                firstWordNum: noteObj.firstWordNum
            };
            dbCRUD.addNote(note).then(function (id) {
                    note.noteId = id;
                    deferred.resolve(note);
                },
                deferred.reject);
            return deferred.promise;
        };

        this.getNotes = function () {
            var curBook = currentBook.getBook();
            return dbCRUD.getNotes(curBook.id);
        };

        this.deleteBookmark = function (note) {
            return dbCRUD.deleteNote(note.id);
        };

        this.findNote = function (note) {
            var pageIndex = 0;
            var pages = currentBook.getPages();
            var tagId = parseInt(note.paragraphNum);

            for (var i = 0; i < pages.length; i++) {
                if (pages[i].startId <= tagId && pages[i].endId >= tagId) {
                    pageIndex = i;
                    break;
                }
            }
            var words;
            //for the span arrays in the div. iterating because the div can be split onto several pages
            for (words = $(pages[pageIndex].pageText).find('section' + tagId).children();
                 words.last().attr('id') < note.firstWordNum;
                 pageIndex++);

            return pageIndex;
        }

        this.findNextNote = function (currentPageIndex, notesList) {
            /****uncomment if list should be extracted here****/
            //var notesList = [];
            //notesList = getNotes();
            notesList.sort(function (a, b) {
                return (a.paragraphNum - b.paragraphNum) + (a.firstWordNum - b.firstWordNum);
            });
            var pages = currentBook.getPages();
            var curPageLastTagId = parseInt($(pages[currentPageIndex]).last().attr('id').substring(7));
            var nextNote = $.grep(notesList, function (note) {
                return note.paragraphNum > curPageLastTagId;
            });
            return nextNote;
        }
    });