angular.module('text.selection', [])
    .service('htmlTextPreparer', function () {
        "use strict";
        var wordNumber;
        var resultHtml;

        var getElementShallowCopy = function (element) {
            var start = '<' + element.localName;
            $.each(element.attributes, function () {
                start += ' ' + this.name + "='" + this.value + "'";
            });
            start += '>';
            var end = '</' + element.localName + '>';

            return {"open": start, "close": end};
        };

        var processElement = function (element) {
            if (element.nodeType === 3) {   // Text node
                var words = element.textContent.split(' ');
                var wrappedText = "";
                for (var i = 0; i < words.length; i++) {
                    wrappedText += "<span id='" + wordNumber + "' class='word'>" + words[i] + " </span>";
                    wordNumber++;
                }
                resultHtml += wrappedText;
            } else if (element.nodeType === 1) {
                var copy = getElementShallowCopy(element);
                resultHtml += copy.open;
                var child;
                for (child = element.firstChild;
                     child;
                     child = child.nextSibling) {
                    processElement(child);
                }
                resultHtml += copy.close;
            }
        };

        this.prepareForSelection = function (htmlText) {
            wordNumber = 0;
            resultHtml = "";
            var elements = $(htmlText);
            if (elements.length > 1) {
                elements.each(function (index, element) {
                    processElement(element);
                });
            } else {
                processElement(elements);
            }
            return resultHtml;
        };
    })
    .service('selectionHandler', function () {
        "use strict";
        var _$scope;

        var _windowSize;

        var _enabled = true;

        var lastCoords = {left: 0, top: 0};

        var spanElements;

        var selectionStart = {
            element : {},
            lastCoords : {},
            num : 0
        };
        var selectionEnd = {
            element : {},
            lastCoords : {},
            num : 0
        };

        var selectElements = function (start, end) {
            for (var i = start; i <= end; i++) {
                spanElements[i].className = "word-selected";
            }
        };

        var unSelectElement = function (start, end) {
            if (spanElements === undefined) {
                return;
            }
            for (var i = start; i <= end; i++) {
                spanElements[i].className = "word";
            }
        };

        this.hide = function () {
            _$scope.showSelection = false;
            unSelectElement(selectionStart.num, selectionEnd.num);
            _$scope.$emit('selectionEnd');
        };

        this.setEnabled = function (enabled) {
            _enabled = enabled;;
        };

        this.setScope = function ($scope) {
            _$scope = $scope;
        };

        this.init = function ($ionicGesture, windowSize) {
            _windowSize = windowSize;
            selectionStart.element = $('#selectionStart');
            selectionEnd.element = $('#selectionEnd');

            _enabled = true;

            _$scope.$on('$destroy', function () {
                $ionicGesture.off(dragStart, 'drag', onStartSelectionDrag);
                $ionicGesture.off(dragEnd, 'drag', onEndSelectionDrag);
            });
            var dragStart = $ionicGesture.on('drag', onStartSelectionDrag, selectionStart.element);
            var dragEnd = $ionicGesture.on('drag', onEndSelectionDrag, selectionEnd.element);

            document.body.addEventListener('touchmove', handleTouchPoint, false);
            document.body.addEventListener('touchstart', handleTouchPoint, false);
            document.body.addEventListener('touchend', handleTouchPoint, false);
        };

        this.getSelectionInfo = function () {
            var text = "";
            for (var i = selectionStart.num; i < selectionEnd.num + 1; i++) {
                text += spanElements[i].innerText;
                if (text[text.length - 1] !== ' ') {
                    text += ' ';
                }
            }
            var parent = spanElements[selectionStart.num].parentNode;
            var num = parent.id.substring(7);
            return {
                selectedText: text,
                paragraphNum: num,
                firstWordNum: selectionStart.num
            };
        };

        var handleTouchPoint = function(event){
            if (event.touches.length === 0) {
                return;
            }
            lastCoords.left = event.touches[0].pageX;
            lastCoords.top  = event.touches[0].pageY;
        };

        this.setPage = function (page) {
            spanElements = page.find('span');
        };

        this.onHold = function () {
            if (_$scope.showSelection || !_enabled) {
                return;
            }
            _$scope.$emit('selectionStart');
            var element = document.elementFromPoint(lastCoords.left, lastCoords.top);
            if (element.localName === 'span') {
                _$scope.showSelection = true;

                selectionStart.num = parseInt(element.id);
                selectionEnd.num = selectionStart.num;

                selectElements(selectionStart.num, selectionStart.num);

                selectionStart.lastCoords = {
                    left : element.offsetLeft - 8,
                    top : element.offsetTop + 8
                };

                selectionEnd.lastCoords = {
                    left : element.offsetLeft + element.offsetWidth - 1,
                    top : element.offsetTop + 8
                };

                selectionStart.element.css(selectionStart.lastCoords);
                selectionEnd.element.css(selectionEnd.lastCoords);

                _$scope.$emit('selectionChanged', { start : selectionStart, end: selectionEnd});
            }
        };

        function findElementInLine(x, y, dir) {
            var border;
            var element;
            if (dir > 0) {
                border = _windowSize.getWindowWidth();
                for (var i = x; i < border; i += 10) {
                    element = document.elementFromPoint(i, y);
                    if (element !== undefined && element.localName === 'span') {
                        return element;
                    }
                }
            } else {
                border = 0;
                for (var i = x; i > border; i -= 10) {
                    element = document.elementFromPoint(i, y);
                    if (element !== undefined && element.localName === 'span') {
                        return element;
                    }
                }
            }
            return undefined;
        }

        var onStartSelectionDrag = function () {
            var startPoint = lastCoords;

            var firstElement = findElementInLine(startPoint.left, startPoint.top, +1);
            if (firstElement === undefined || firstElement.localName === undefined) {
                selectionStart.element.css(selectionStart.lastCoords);
                return;
            }

            var currentNum = parseInt(firstElement.id);
            if (currentNum > selectionEnd.num) {
                selectionStart.element.css(selectionStart.lastCoords);
                return;
            }

            if (currentNum === selectionStart.num) {
                return;
            }

            selectionStart.lastCoords = {
                left: firstElement.offsetLeft - 8,
                top: firstElement.offsetTop + 8
            };
            selectionStart.element.css(selectionStart.lastCoords);

            if (selectionStart.num < currentNum) {
                unSelectElement(selectionStart.num, currentNum - 1);
            } else {
                selectElements(currentNum, selectionStart.num);
            }
            selectionStart.num = currentNum;
//            console.log("Selecting from pos: " + selectionStart.num + " to: " + selectionEnd.num);

            _$scope.$emit('selectionChanged', { start : selectionStart, end: selectionEnd});
        };

        var onEndSelectionDrag = function () {
            var endPoint = lastCoords;

            var lastElement = findElementInLine(endPoint.left, endPoint.top, -1);
            if (lastElement === undefined || lastElement.localName === undefined) {
                selectionEnd.element.css(selectionEnd.lastCoords);
                return;
            }

            var currentNum = parseInt(lastElement.id);
            if (currentNum < selectionStart.num) {
                selectionEnd.element.css(selectionEnd.lastCoords);
                return;
            }

            if (currentNum === selectionEnd.num) {
                return;
            }

            selectionEnd.lastCoords = {
                left: lastElement.offsetLeft + lastElement.offsetWidth,
                top: lastElement.offsetTop + 8
            };
            selectionEnd.element.css(selectionEnd.lastCoords);

            if (selectionEnd.num > currentNum) {
                unSelectElement(currentNum + 1, selectionEnd.num);
            } else {
                selectElements(selectionEnd.num, currentNum);
            }
            selectionEnd.num = currentNum;
//            console.log("Selecting from pos: " + selectionStart.num + " to: " + selectionEnd.num);

            _$scope.$emit('selectionChanged', { start : selectionStart, end: selectionEnd});
        };
    });