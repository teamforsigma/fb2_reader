/**
 * Created by Home on 11/3/2014.
 */
angular.module('nameComparer', [])

    .service('nameComparer', function() {

        var costs = {};

        // copy and pasta
        /**
         * @param {string} s1 origin string
         * @param {string} s2 string to compare
         * @param {object} [costs] operation costs { [replace], [replaceCase], [insert], [remove] }
         * @return {number} Levenstein's distance
         */
        var levenstein = function(s1, s2, costs) {
            var i, j, l1, l2, flip, ch, chl, ii, ii2, cost, cutHalf;
            l1 = s1.length;
            l2 = s2.length;

            costs = costs || {};
            var cr = costs.replace || 1;
            var cri = costs.replaceCase || costs.replace || 1;
            var ci = costs.insert || 1;
            var cd = costs.remove || 1;

            cutHalf = flip = Math.max(l1, l2);

            var minCost = Math.min(cd, ci, cr);
            var minD = Math.max(minCost, (l1 - l2) * cd);
            var minI = Math.max(minCost, (l2 - l1) * ci);
            var buf = new Array((cutHalf * 2) - 1);

            for (i = 0; i <= l2; ++i) {
                buf[i] = i * minD;
            }

            for (i = 0; i < l1; ++i, flip = cutHalf - flip) {
                ch = s1[i];
                chl = ch.toLowerCase();

                buf[flip] = (i + 1) * minI;

                ii = flip;
                ii2 = cutHalf - flip;

                for (j = 0; j < l2; ++j, ++ii, ++ii2) {
                    cost = (ch === s2[j] ? 0 : (chl === s2[j].toLowerCase()) ? cri : cr);
                    buf[ii + 1] = Math.min(buf[ii2 + 1] + cd, buf[ii] + ci, buf[ii2] + cost);
                }
            }
            return buf[l2 + cutHalf - flip];
        }

        this.compare = function(author1, author2) {
            var a1 = {
                    firstName : author1.firstName || '',
                    lastName : author1.lastName || ''
                },
                a2 = {
                    firstName : author2.firstName || '',
                    lastName : author2.lastName || ''
                }

            var distance =
                levenstein(a1.firstName, a2.firstName, costs) +
                levenstein(a1.lastName, a2.lastName, costs);

            return distance;
        }
    });