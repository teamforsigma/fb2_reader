/**
 * Created by Dmitryy on 28-Oct-14.
 */
angular.module('dropboxManager', [])

    .service('dropbox', function ($q) {

        this.listDir = function(dir) {
            var deferred = $q.defer();

            DropboxSync.checkLink(function () {
                DropboxSync.listFolder('/', function (files) { // success
                    /* Each object in files has properties: path, modifiedTime, size, and isFolder.*/
                    deferred.resolve(files);
                }, function() {
                    console.log('dropbox cant list dir')
                    deferred.reject();
                });
            }, function() {
                 deferred.reject('not logged in');
            });
            return deferred.promise;
        };

        this.getFile = function(dropboxPath) {
            return false;
        }




        //does nothing
//        this.launchDropbox = function () {
//            window.plugins.webintent.launchApp(
//                'com.dropbox.android',
//                function () {
//                    console.log('launched activity');
//                },
//                function () {
//                    console.log('failed launch activity');
//                }
//            );
//
//        };


        // may be used to check if app was launched to open a file
        this.checkExtras = function () {
            window.plugins.webintent.hasExtra(window.plugins.webintent.EXTRA_TEXT,
                function (uri) {
                    //open book with this uri
                    console.log('extras uri: ' + uri);
                },
                function () {
                    console.log('no extras')
                });
        };

        this.checkUri = function () {
            window.plugins.webintent.getUri(function (url) {
                if (url !== "") {
                    // url is the url the intent was launched with
                    console.log('webintent url: ' + url);
                } else {
                    console.log('webintent has no url')
                }
            });
        };



    });