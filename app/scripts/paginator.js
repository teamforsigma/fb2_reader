angular.module('pages.service', [])
    .service('paging', function () {

        this.getPages = function (fontSizePx, text, window) {
            var fontHeight = fontSizePx;
            var fontWidth = fontSizePx * 0.6;
            var interval = 0.25 * fontHeight;// Math.ceil(0.75 * fontHeight);

            var rowHeight = fontHeight + interval;
            var rows = Math.ceil(((window.height - 2 * window.topPadding) / rowHeight));
            var symInRow = Math.floor(((window.width - 2 * window.leftPadding) / fontWidth));

            /*{
             pageText = html
             startId
            }*/
            var pages = [];
            var curRows = rows; //rows left to fill
            var curPage = '';

            var getSizeInRows = function (t) {
                return Math.ceil(t.length / symInRow);
            };
            var reset = function () {
                curPage = '';
                curRows = rows;
            };
            var finishPage = function () {
                pages.push({
                    pageText: curPage,
                    startId: '',
                    endId: ''
                });
                reset();
            };
            //level is times h text is bigger than current
            var hExecute = function (level, obj, isNested) {
                var element = $(isNested ?
                    obj.children().first() :
                    obj);
                processTextElement(element, level);
            };

            var processTextElement = function (element, symbolRelativeWidth, inline) {
                if (typeof(inline) === 'undefined') inline = false;

                element = $(element);
                var text = element.text();
                element.html("");
                var row = "";
                var currentElement;
                var words = text.split(' ');
                var symbolsInCurrentRow = 0;
                var lastWordPageEnd = 0;
                for (var i = 0; i < words.length; i++) {
                    var word = words[i];
                    var wordLength = word.length * symbolRelativeWidth;
                    if (symbolsInCurrentRow + wordLength > symInRow) {
                        // Word can we splitted by -
                        if (symbolsInCurrentRow === 0) {
                            // Fixme: Slice this word
                            i++;
                            continue;
                        }
                        var wordParts = word.split('-');
                        if (wordParts.length > 1) {
                            if (symbolsInCurrentRow + wordParts[0].length * symbolRelativeWidth + symbolRelativeWidth > symInRow) {

                            } else {
                                var cLength = 0;
                                for (var j = 0; j < wordParts.length; j++) {
                                    cLength += wordParts[j].length + symbolRelativeWidth;
                                    if (symbolsInCurrentRow + cLength > symInRow) {
                                        var firstPart = wordParts.slice(0, j).join('-') + '-';
                                        var secondPart = wordParts.slice(j).join('-');
                                        words[i] = firstPart;
                                        words.splice(i + 1, 0, secondPart);
                                        break;
                                    }
                                }
                                i--;
                                continue;
                            }
                        }
                        curRows -= symbolRelativeWidth;
                        if (curRows < 1) {
                            currentElement = element.clone();
                            currentElement.html(words.slice(lastWordPageEnd, i).join(' '));
                            lastWordPageEnd = i;
                            curPage += currentElement[0].outerHTML;

                            finishPage();
                        }
                        symbolsInCurrentRow = 0;
                        i--;
                    } else {
                        symbolsInCurrentRow += wordLength + symbolRelativeWidth;
                    }
                }

                if (lastWordPageEnd !== words.length - 1) {
                    currentElement = element.clone();
                    currentElement.html(words.slice(lastWordPageEnd).join(' '));
                    curPage += currentElement[0].outerHTML;
                }

                if (!inline) {
                    if (lastWordPageEnd !== words.length - 1) {
                        if (symbolsInCurrentRow !== 0) {
                            curRows -= symbolRelativeWidth;
                        }
                        curRows -= 0.5 * symbolRelativeWidth;
                    }
                }

                if (curRows < 1) {
                    finishPage();
                }
            };
            var br = function () {
                if (curRows <= 1) {
                    finishPage();
                } else {
                    curRows--;
                    curPage += '<br/>';
                }
            };

            var html = $(text);
            html.each(function () {
                switch (this.localName) {
                    case 'p' :
                        if ($(this).find("p").length > 0) {
                            $(this).find("p").forEach(function () {
                                processTextElement(this, 1);
                            });
                        } else if ($(this).find("br").length > 0) {
                            br();
                        } else if ($(this).find("h1").length > 0) {
                            hExecute(1, $(this).find("h1"), true);
                        } else {
                            processTextElement(this, 1);
                        }
                        break;

                    case 'a' :
                        //contains an 'h ' tag
                        if ($(this).children().length > 0) {
                            var level = parseInt($(this).find("h").prop("localName").substring(1)); //gets the level of h tag
                            hExecute(level, this, true);
                        } else {
                            processTextElement(this, 1);
                        }
                        break;

                    case 'br' :
                        br();
                        break;

                    //break page if chapter
                    case 'h1' :
                        if (curRows !== rows) {
                            finishPage();
                        }
                        hExecute(2, this, false);
                        break;

                    case 'h2' :
                        hExecute(1.5, this, false);
                        break;

                    case 'h3' :
                        hExecute(1.17, this, false);
                        break;

                    case 'h4' :
                        hExecute(1, this, false);
                        break;

                    case 'h5' :
                        hExecute(0.83, this, false);
                        break;

                    case 'h6' :
                        hExecute(0.67, this, false);
                        break;

                    //image
                    case 'div' :
                        var imgTag = $(this).find("img");
                        var i = new Image();
                        i.src = imgTag.attr("src");
                        while (1) {
                            if (i.complete) {
                                break;
                            }
                        }
                        //rescale
                        if (i.width > (window.width - 2 * window.leftPadding) ||
                            i.height > (window.height - 2 * window.topPadding)) {
                            var scaleRatio = Math.max(
                                    i.width / (window.width - 2 * window.leftPadding),
                                    i.height / (window.height - 2 * window.topPadding)
                            );
                            i.height /= scaleRatio;
                            i.width /= scaleRatio;
                        }
                        var sizeInRows = Math.ceil(i.height / rowHeight);
                        if (sizeInRows > curRows) {
                            finishPage();
                        }
                        imgTag.attr('height', i.height);
                        imgTag.attr('width', i.width);
                        curPage += this.outerHTML;
                        curRows -= sizeInRows;
                        break;
                }
            });

            return this.indexPages(pages);
        };

        this.indexPages = function(pages) {
            $(pages).each(function() {
                var page = $(this.pageText);
                    this.startId = parseInt(page.first().attr('id').substring(7));
                    this.endId = parseInt(page.last().attr('id').substring(7));//skip section words

            });
            return pages;
        };
    })