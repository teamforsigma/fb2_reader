angular.module('db.manager', [])
    .factory('db', function ($q) {
        var deferred = $q.defer();

        function populateDB(tx) {
            var deferred = $q.defer();
            var tables = 5;
            var onSuccess = function () {
                tables--;
                if (tables === 0) {
                    deferred.resolve();
                }
            };

            db.transaction(function (tx) {
                tx.executeSql('CREATE TABLE IF NOT EXISTS AUTHOR (' +                               //author table creation
                    'authorId INTEGER PRIMARY KEY, ' +
                    'firstName VARCHAR(25) NOT NULL, ' +
                    'middleName VARCHAR(25), ' +
                    'lastName VARCHAR (30) NOT NULL, ' +
                    'nickname VARCHAR(25));', onSuccess, function (err) {
                    console.log('author');
                    deferred.reject(err);
                });

                tx.executeSql('CREATE TABLE IF NOT EXISTS BOOK (' +                                     //book table creation
                    'id INTEGER PRIMARY KEY,' +
                    'ISBN VARCHAR(20), ' +
                    'authorId INTEGER NOT NULL, ' +
                    'genre VARCHAR(50), ' +
                    'title VARCHAR(255) NOT NULL, ' +
                    'releaseDate DATE, ' +
                    'annotation VARCHAR(2000), ' +                                                  //OR MORE. IDK
                    'encoding VARCHAR(10) NOT NULL, ' +
                    'lastReadPage INTEGER, ' +
                    'fileName VARCHAR(1024), ' +
                    'rate INTEGER, ' +
                    'coverpageExtension VARCHAR(6), ' +
                    'lastOpenDate DATE);', onSuccess, function (err) {
                    console.log('book');
                    deferred.reject(err);
                });

                tx.executeSql('CREATE TABLE IF NOT EXISTS BOOKMARK (' +                             //bookmark table creation
                    'bookmarkId INTEGER PRIMARY KEY, ' +
                    'bookId INTEGER, ' +
                    'name VARCHAR(50) NOT NULL, ' +
                    'position INTEGER NOT NULL);', onSuccess, function (err) {
                    console.log('bookmark');
                    deferred.reject(err);
                });

                tx.executeSql('CREATE TABLE IF NOT EXISTS NOTE (' +                                 //note table creation
                    'noteId INTEGER PRIMARY KEY, ' +
                    'bookId INTEGER, ' +
                    'name VARCHAR(50) NOT NULL, ' +
                    'comment VARCHAR(255) NOT NULL, ' +
                    'paragraphNum INTEGER NOT NULL, ' +
                    'firstWordNum INTEGER NOT NULL);', onSuccess, function (err) {
                    console.log('note');
                    deferred.reject(err);
                });

                tx.executeSql('CREATE TABLE IF NOT EXISTS BOOK_AUTHOR(' +
                    'connectionId INTEGER PRIMARY KEY,' +
                    'bookId INTEGER,' +
                    'authorId INTEGER);', onSuccess, function (err) {
                    console.log('book_author');
                    deferred.reject(err);
                });
            });

            return deferred.promise;
        }

        var db = window.openDatabase("fb2ReaderDB", "1.0", "Fb2 Reader DB", 1000000);//don't know if the size of db is right
        db.whenInitialize = deferred.promise;
        db.transaction(
            function (tx) {
                tx.executeSql("SELECT name FROM sqlite_master WHERE type='table' AND name='BOOK'",
                    [], function (tx, result) {                                                     //not sure about request
                        if (result.rows.length === 0) {                                                 //hell knows what's going on
                            try {
                                populateDB().then(deferred.resolve, deferred.reject);
                            }
                            catch (e) {
                                deferred.reject(e);
                            }
                        } else {
                            console.log("Db exists");
                            deferred.resolve();
                        }
                    }, function (err) {
                        deferred.reject(err);
                        console.log("Error checking db");
                    });
            }
        );
        deferred.promise.then(function () {
        }, function (err) {
            console.log("Error while creating db " + err.message);
        });
        db.whenInitialize = deferred.promise;
        return db;
    });
