angular.module('db.functions', ['db.manager'])
    .service('dbCRUD', function (db, $q) {
        var getLastInsertedID = function (tableName, id) {//gets the id of table
            var deferred = $q.defer();
            db.transaction(function (tx) {
                try {
                    tx.executeSql("SELECT max(" + id + ") as maxId FROM " + tableName + ";", [],
                        function (tx, results) {
                            deferred.resolve(results.rows.item(0).maxId);
                            console.log("last inserted id: " + results.rows.item(0).maxId);
                        });
                } catch (ex) {
                    console.log('error in db (last inserted) : ' + ex)
                    deferred.reject(ex);
                }
            });
            return deferred.promise;
        };

        var getLastInsertedAuthorId = function () {
            return getLastInsertedID('AUTHOR', 'authorId');
        };

        var getLastInsertedBook = function () {
            return getLastInsertedID('BOOK', 'id');
        };

        var getLastInsertedBookmark = function () {
            return getLastInsertedID('BOOKMARK', 'bookmarkId');
        };

        var getLastInsertedNote = function () {
            return getLastInsertedID('NOTE', 'noteId');
        };

        this.addBook = function (book) {
            var deferred = $q.defer();
            db.transaction(function (tx) {
                try {
                    tx.executeSql("INSERT INTO BOOK (ISBN, authorId, genre, title, releaseDate," +
                            "annotation, encoding, lastReadPage, rate, lastOpenDate, fileName, coverpageExtension) VALUES " +
                            "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [book.ISBN, book.authorId, book.genre,
                            book.title, book.releaseDate, book.annotation, book.encoding, book.lastReadPage,
                            book.rate, book.lastOpenDate, book.fileName, book.coverpageExtension],
                        function (tx, results) {
                            getLastInsertedBook().then(deferred.resolve, deferred.reject);
                        });
                } catch (ex) {
                    deferred.reject(ex);
                }
            });
            return deferred.promise;
        };

        this.addAuthor = function (author) {
            var deferred = $q.defer();
            db.transaction(function (tx) {
                try {
                    tx.executeSql("INSERT INTO AUTHOR (firstName, middleName, lastName, nickname) VALUES" +
                            "(?, ?, ?, ?)", [author.firstName, author.middleName,
                            author.lastName, author.nickname],
                        function (tx, results) {
                            getLastInsertedAuthorId().then(deferred.resolve, deferred.reject);
                        });
                } catch (ex) {
                    deferred.reject(ex);
                }
            });
            return deferred.promise;
        };

        this.addBookmark = function (bookmark) {
            var deferred = $q.defer();
            db.transaction(function (tx) {
                try {
                    tx.executeSql("INSERT INTO BOOKMARK (bookId, name, position) VALUES" +
                            "(?, ?, ?)", [bookmark.bookId, bookmark.name, bookmark.position],
                        function (tx, results) {
                            getLastInsertedBookmark().then(deferred.resolve, deferred.reject);
                        });
                } catch (ex) {
                    deferred.reject();
                }
            });
            return deferred.promise;
        };

        this.addNote = function (note) {
            var deferred = $q.defer();
            db.transaction(function (tx) {
                try {
                    tx.executeSql("INSERT INTO NOTE (bookId, name, comment, paragraphNum, firstWordNum) VALUES" +
                            "(?, ?, ?, ?, ?)", [note.bookId, note.name, note.comment, note.paragraphNum, note.firstWordNum],
                        function (tx, results) {
                            getLastInsertedNote().then(deferred.resolve, deferred.reject);
                        });
                } catch (ex) {
                    console.log('error in db (add note) : ' + ex);
                    deferred.reject();
                }
            });
            return deferred.promise;
        };

        this.addAuthorToBook = function (author, book) {//change functionality because of table change
            var deferred = $q.defer();
            db.transaction(function (tx) {
                try {
                    tx.executeSql("INSERT INTO BOOK_AUTHOR (bookId, authorId) VALUES" +
                        "(?, ?)", [book.id, author.authorId]);
                    deferred.resolve();
                } catch (ex) {
                    deferred.reject();
                }
            });
            return deferred.promise;
        };

        this.getBooks = function () {
            var deferred = $q.defer();
            var list = [];
            db.transaction(function (tx) {
                tx.executeSql("SELECT * FROM BOOK", [],
                    function (tx, result) {
                        var dataLength = result.rows.length;
                        if (dataLength > 0) {
                            for (var i = 0; i < result.rows.length; i++) {
                                var item = {};
                                item.id = result.rows.item(i).id;
                                item.ISBN = result.rows.item(i).ISBN;
                                item.authorId = result.rows.item(i).authorId;
                                item.genre = result.rows.item(i).genre;
                                item.title = result.rows.item(i).title;
                                item.releaseDate = result.rows.item(i).releaseDate;
                                item.annotation = result.rows.item(i).annotation;
                                item.encoding = result.rows.item(i).encoding;
                                item.lastReadPage = result.rows.item(i).lastReadPage;
                                item.rate = result.rows.item(i).rate;
                                item.lastOpenDate = new Date(result.rows.item(i).lastOpenDate);
                                item.fileName = result.rows.item(i).fileName;
                                item.coverpageExtension = result.rows.item(i).coverpageExtension;
                                list.push(item);
                            }
                        }
                        deferred.resolve(list);
                    },
                    function (error) {
                        deferred.reject();
                    });
            });
            return deferred.promise;
        };

        this.getGenres = function () {
            var deferred = $q.defer();
            var list = [];
            db.transaction(function (tx) {
                tx.executeSql("SELECT DISTINCT BOOK.genre FROM BOOK", [],
                    function (tx, result) {
                        var dataLength = result.rows.length;
                        if (dataLength > 0) {
                            for (var i = 0; i < result.rows.length; i++) {
                                list.push(result.rows.item(i));
                            }
                        }
                        deferred.resolve(list);
                    },
                    function (error) {
                        deferred.reject();
                    });
            });
            return deferred.promise;
        };

        this.getAuthors = function () {
            var deferred = $q.defer();
            var list = [];
            db.transaction(function (tx) {
                tx.executeSql("SELECT  * FROM AUTHOR", [],
                    function (tx, result) {
                        var dataLength = result.rows.length;
                        if (dataLength > 0) {
                            for (var i = 0; i < result.rows.length; i++) {
                                var item = {};
                                item.authorId = result.rows.item(i).authorId;
                                item.firstName = result.rows.item(i).firstName;
                                item.middleName = result.rows.item(i).middleName;
                                item.lastName = result.rows.item(i).lastName;
                                item.nickname = result.rows.item(i).nickname;
                                list.push(item);
                            }
                        }
                        deferred.resolve(list);
                    },
                    deferred.reject
                );
            });
            return deferred.promise;
        };

        this.getBookmarks = function (bookId) {
            var deferred = $q.defer();
            var list = [];
            db.transaction(function (tx) {
                tx.executeSql("SELECT  * FROM BOOKMARK WHERE bookId = ? ", [bookId],
                    function (tx, result) {
                        var dataLength = result.rows.length;
                        if (dataLength > 0) {
                            for (var i = 0; i < result.rows.length; i++) {
                                var item = {};
                                item.bookmarkId = result.rows.item(i).bookmarkId;
                                item.bookId = result.rows.item(i).bookId;
                                item.name = result.rows.item(i).name;
                                item.position = result.rows.item(i).position;
                                list.push(item);
                            }
                        }
                        deferred.resolve(list);
                    },
                    function (error) {
                        deferred.reject();
                    });
            });
            return deferred.promise;
        };

        this.getNotes = function (bookId) {
            var deferred = $q.defer();
            var list = [];
            db.transaction(function (tx) {
                tx.executeSql("SELECT  * FROM NOTE WHERE bookId = ?", [bookId],
                    function (tx, result) {
                        var dataLength = result.rows.length;
                        if (dataLength > 0) {
                            for (var i = 0; i < result.rows.length; i++) {
                                var item = {};
                                item.noteId = result.rows.item(i).noteId;
                                item.bookId = result.rows.item(i).bookId;
                                item.name = result.rows.item(i).name;
                                item.comment = result.rows.item(i).comment;
                                item.paragraphNum = result.rows.item(i).paragraphNum;
                                item.firstWordNum = result.rows.item(i).firstWordNum;
                                list.push(item);
                            }
                        }
                        deferred.resolve(list);
                    },
                    function (error) {
                        deferred.reject();
                    });
            });
            return deferred.promise;
        };

        this.getAuthorsOfBooks = function () {
            var deferred = $q.defer();
            var list = [];
            db.transaction(function (tx) {
                tx.executeSql("SELECT * FROM BOOK_AUTHOR", [],
                    function (tx, result) {
                        var dataLength = result.rows.length;
                        if (dataLength > 0) {
                            for (var i = 0; i < result.rows.length; i++) {
                                var item = {};
                                item.bookId = result.rows.item(i).bookId;
                                item.authorId = result.rows.item(i).authorId;
                                list.push(item);
                            }
                        }
                        deferred.resolve(list);
                    },
                    function (error) {
                        deferred.reject();
                    });
            });
            return deferred.promise;
        };

        this.getBookInfo = function (bookId) {
            var deferred = $q.defer();
            var book = {};
            db.transaction(function (tx) {
                tx.executeSql("SELECT * FROM BOOK WHERE id = ?", [bookId],
                    function (tx, result) {
                        var dataLength = result.rows.length;
                        if (dataLength > 0) {
                            book.id = bookId;
                            book.ISBN = result.rows.item(0).ISBN;
                            book.authorId = result.rows.item(0).authorId;
                            book.genre = result.rows.item(0).genre;
                            book.title = result.rows.item(0).title;
                            book.releaseDate = result.rows.item(0).releaseDate;
                            book.annotation = result.rows.item(0).annotation;
                            book.encoding = result.rows.item(0).encoding;
                            book.lastReadPage = result.rows.item(0).lastReadPage;
                            book.rate = result.rows.item(0).rate;
                            book.lastOpenDate = result.rows.item(0).lastOpenDate;
                        }
                        deferred.resolve(book);
                    },
                    function (error) {
                        deferred.reject();
                    });
            });
            return deferred.promise;
        };

        this.getAuthorInfo = function (authorId) {
            var deferred = $q.defer();
            var author = {};
            db.transaction(function (tx) {
                tx.executeSql("SELECT  * FROM AUTHOR WHERE authorId = ?", [authorId],
                    function (tx, result) {
                        var dataLength = result.rows.length;
                        if (dataLength > 0) {
                            author.authorId = authorId;
                            author.firstName = result.rows.item(0).firstName;
                            author.middleName = result.rows.item(0).middleName;
                            author.lastName = result.rows.item(0).lastName;
                            author.nickname = result.rows.item(0).nickname;
                        }
                        deferred.resolve(author);
                    },
                    function (error) {
                        deferred.reject();
                    });
            });
            return deferred.promise;
        };

        this.getBookmarkInfo = function (bookmarkId) {
            var deferred = $q.defer();
            var bookmark = {};
            var list = [];
            db.transaction(function (tx) {
                tx.executeSql("SELECT  * BOOKMARK WHERE bookmarkId = ?", [bookmarkId],
                    function (tx, result) {
                        var dataLength = result.rows.length;
                        if (dataLength > 0) {
                            bookmark.bookmarkId = bookmarkId;
                            bookmark.bookId = result.rows.item(0).bookId;
                            bookmark.name = result.rows.item(0).name;
                        }
                        deferred.resolve(bookmark);
                    },
                    function (error) {
                        deferred.reject();
                    });
            });
            return deferred.promise;
        };

        this.getNoteInfo = function (noteId) {
            var deferred = $q.defer();
            var note = {};
            var list = [];
            db.transaction(function (tx) {
                tx.executeSql("SELECT  * FROM NOTE WHERE noteId = ?", [noteId],
                    function (tx, result) {
                        var dataLength = result.rows.length;
                        if (dataLength > 0) {
                            note.noteId = noteId;
                            note.bookId = result.rows.item(0).bookId;
                            note.name = result.rows.item(0).name;
                            note.comment = result.rows.item(0).comment;
                        }
                        deferred.resolve(note);
                    },
                    function (error) {
                        deferred.reject();
                    });
            });
            return deferred.promise;
        };

        this.updateBook = function (book) {
            var deferred = $q.defer();
            db.transaction(function (tx) {
                tx.executeSql("UPDATE BOOK SET ISBN = ?, authorId = ?, genre = ?, title = ?, releaseDate = ?," +
                        "annotation = ?, encoding = ?, lastReadPage = ?, rate = ?, lastOpenDate = ?, fileName = ?, coverpageExtension = ?" +
                        "WHERE id = ?", [book.ISBN, book.authorId, book.genre, book.title, book.releaseDate,
                        book.annotation, book.encoding, book.lastReadPage, book.rate, book.lastOpenDate, book.fileName, book.coverpageExtension, book.id],
                    function (tx, result) {
                        console.log("dbCRUD.updateBook -> " + book.title + " " + book.lastOpenDate);
                        deferred.resolve();
                    },
                    function (error) {
                        deferred.reject();
                    });
            });
            return deferred.promise;
        };

        this.updateAuthor = function (author) {
            var deferred = $q.defer();
            db.transaction(function (tx) {
                tx.executeSql("UPDATE AUTHOR SET firstName = ?, middleName = ?, lastName = ?, nickname = ? WHERE" +
                        "authorId = ?", [author.firstName, author.middleName, author.lastName, author.nickname,
                        author.authorId],
                    function (tx, result) {
                        deferred.resolve();
                    },
                    function (error) {
                        deferred.reject();
                    });
            });
            return deferred.promise;
        };

        this.updateBookmark = function (bookmark) {
            var deferred = $q.defer();
            db.transaction(function (tx) {
                tx.executeSql("UPDATE BOOKMARK SET bookId = ?, name = ?, position = ? WHERE bookmarkId = ?",
                    [bookmark.bookId, bookmark.name, bookmark.position, bookmark.bookmarkId],
                    function (tx, result) {
                        deferred.resolve();
                    },
                    function (error) {
                        deferred.reject();
                    });
            });
            return deferred.promise;
        };

        this.updateNote = function (note) {
            var deferred = $q.defer();
            db.transaction(function (tx) {
                tx.executeSql("UPDATE NOTE SET bookId = ?, name = ?, comment = ?, paragraphNum = ?, firstWordNum = ? WHERE noteId = ?",
                    [note.bookId, note.name, note.comment, note.paragraphNum, note.firstWordNum, note.noteId],
                    function (tx, result) {
                        deferred.resolve();
                    },
                    function (error) {
                        deferred.reject();
                    });
            });
            return deferred.promise;
        };

        this.deleteBook = function (book_id) {
            var deferred = $q.defer();
            db.transaction(function (tx) {
                tx.executeSql("DELETE FROM BOOK WHERE id = ?", [book_id],
                    function (tx, result) {
                        deferred.resolve();
                    },
                    function (error) {
                        deferred.reject();
                    });
            });
            return deferred.promise;
        };

        this.deleteAuthor = function (author_id) {
            var deferred = $q.defer();
            db.transaction(function (tx) {
                tx.executeSql("DELETE FROM AUTHOR WHERE authorId = ?", [author_id],
                    function (tx, result) {
                        deferred.resolve();
                    },
                    function (error) {
                        deferred.reject();
                    });
            });
            return deferred.promise;
        };

        this.deleteBookmark = function (bookmark_id) {
            var deferred = $q.defer();
            db.transaction(function (tx) {
                tx.executeSql("DELETE FROM BOOKMARK WHERE bookmarkId = ?", [bookmark_id],
                    function (tx, result) {
                        deferred.resolve();
                    },
                    function (error) {
                        deferred.reject();
                    });
            });
            return deferred.promise;
        };

        this.deleteNote = function (note_id) {
            var deferred = $q.defer();
            db.transaction(function (tx) {
                tx.executeSql("DELETE FROM NOTE WHERE noteId = ?", [note_id],
                    function (tx, result) {
                        deferred.resolve();
                    },
                    function (error) {
                        deferred.reject();
                    });
            });
            return deferred.promise;
        };

        this.getDB = function () {
            return db;
        }
    });
