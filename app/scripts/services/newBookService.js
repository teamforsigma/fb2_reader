angular.module('newBookModule', [])
    .service("newBookService", function ($rootScope, $q, fb2Reader, fileMover, libraryData, appStorage) {
        this.addNewBook = function (file) {
            $rootScope.$emit("openBookStart");
            var deferred = fb2Reader.read(file).then(function (book) {
                return fileMover.moveToLocalLibrary(file).then(function () {
                    var coverpage = book.description.titleInfo.coverpage || book.description.srcTitleInfo.coverpage;
                    var coverpageBinary;
                    book.coverpageExtension = "";
                    if (coverpage !== undefined && coverpage !== "") {
                        $.each(book.binary, function (index, binary) {
                            if (binary.id === coverpage) {
                                coverpageBinary = binary;
                            }
                        });
                        if (coverpageBinary !== undefined) {
                            book.coverpageExtension = coverpageBinary.contentType.split('/')[1];
                        }
                    }

                    return libraryData.openNewBook(book).then(function (bookMeta) {
                        return appStorage.saveBookCoverpage(bookMeta, coverpageBinary).then(function () {
                            return libraryData.updateBookCoverpage(bookMeta).then(function () {
                                return bookMeta;
                            });
                        });
                    });
                });

            });
            deferred.then(function () {
                $rootScope.$emit("openBookEnd");
            }, function () {
                $rootScope.$emit("openBookEnd");
            });
            return deferred;
        }
    });
