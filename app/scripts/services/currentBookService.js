angular.module('currentBookTracker', ['pages.service', 'settingsHolder', 'parameters', 'libraryDataHolder', 'db.manager', 'utils.fs.reader', 'fb2.parser', 'dropboxManager'])

    .service('currentBook', function ($rootScope, paging, userSettings, windowParams, libraryData,  appStorage, fb2Parser, $q, dropbox, $ionicLoading) {
        var currentBook;
        var currentBookPages = [];

        //todo: save last read page to the book before chaning
        var setBookLocal = function (book) {
            var deferred = $q.defer();

            appStorage.getBook(book.fileName, book.encoding).then(function (xml) {
                fb2Parser.parseFb2(xml).then(function (parsedBook) {
                    parsedBook.encoding = book.encoding;
                    parsedBook.fileName = book.fileName;
                    currentBookPages = paging.getPages(userSettings.getSettings().fontSize.number, parsedBook.body[0], windowParams);
                    currentBook = parsedBook;
                    libraryData.openNewBook(parsedBook).then(
                        function (res) {
                            currentBook = res;
                            deferred.resolve();
                        },
                        function (err) {
                            console.log('error in set book: ' + err.message);
                        });
                });
            });
            return deferred.promise;
        };
        this.setBookLocal = setBookLocal;

        this.getBook = function () {
            return currentBook;
        };

        this.getPages = function () {
            return currentBookPages;
        };

        this.updateCurrentPage = function (lastReadPageIndex) {
            currentBook.lastReadPage = lastReadPageIndex;
            return libraryData.updateLastReadPage(currentBook.id, lastReadPageIndex);
        };

        var init = function () {
            var lastReadBook = libraryData.getLastOpenedBook();
            if (lastReadBook !== null) {
                $ionicLoading.show({
                    template: '<h2 class="icon ion-loading-c"></h2>',
                    animation: 'fade-in',
                    noBackdrop: false
                });
                setBookLocal(lastReadBook).then(function () {
                    $rootScope.$emit('setBook');
                    $ionicLoading.hide();
                });
            }
        };

        this.rescaleText = function() {
            var lastReadBook = libraryData.getLastOpenedBook();
            if (lastReadBook !== null) {
                setBookLocal(lastReadBook).then(function () {
                });
            };
        }

        libraryData.ready.then(init);
    });