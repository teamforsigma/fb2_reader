angular.module('intentModule', ['ionic', 'newBookModule', 'utils.fs.reader'])

    .service('intentService', function ($ionicPlatform, $state, $rootScope, externalFileResolver, newBookService) {

        var openBook = function (uri) {
            externalFileResolver.resolve(uri).then(function(fileInfo) {
                $state.go('menu.libraryTabs.title');
                newBookService.addNewBook(fileInfo.file);
            }, function (err) {
                console.log("Failed to open file from extras");
            });
        };

        var checkExtras = function () {
            window.plugins.webintent.getExtra(window.plugins.webintent.ACTION_VIEW,
                function (extras) {
                    console.log('Started with extras: ' + extras);
                    openBook(extras);
                },
                function () {
                    console.log('Started with no extras');
                });
        };

        $ionicPlatform.ready(function () {
            window.plugins.webintent.onNewIntent(function (extras) {
                console.log("Passed extras: " + extras);
                openBook(extras);
            });

            checkExtras();
        });
    });
