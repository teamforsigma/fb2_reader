angular.module('utils', [])
    // Make html trusted, so that nothing will be stripped out
    .filter('to_trusted', ['$sce', function ($sce) {
        return function (text) {
            return $sce.trustAsHtml(text);
        };
    }]);