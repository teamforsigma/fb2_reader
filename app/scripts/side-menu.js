angular.module('sideMenu.controllers', ['ionic'])

    .controller('SideMenuController', function ($scope, $ionicSideMenuDelegate) {
        "use strict";
        $scope.toggleLeft = function() {
            $ionicSideMenuDelegate.toggleLeft();
        };

        $scope.fullScreenMode = false;

        $scope.changeFullScreenMode = function() {
            $scope.fullScreenMode = $scope.fullScreenMode !== true;
        };

        $scope.setFullScreenMode = function (enable) {
            $scope.fullScreenMode = enable;
        }
    });


