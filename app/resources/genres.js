/**
 * Created by Constantine on 02.11.2014.
 */
var genres = [
    {
        "Key":"sf_history",
        "En":"Alternative History",
        "Rus":"Альтернативная история"
    },
    {
        "Key":"sf_action",
        "En":"Fighting Fantasy",
        "Rus":"Боевая фантастика"
    },
    {
        "Key":"sf_epic",
        "En":"Epic Fiction",
        "Rus":"Эпическая фантастика"
    },
    {
        "Key":"sf_heroic",
        "En":"Heroic Fantasy",
        "Rus":"Героическая фантастика"
    },
    {
        "Key":"sf_detective",
        "En":"Detective Fiction",
        "Rus":"Детективная фантастика"
    },
    {
        "Key":"sf_cyberpunk",
        "En":"Cyberpunk",
        "Rus":"Киберпанк"
    },
    {
        "Key":"sf_space",
        "En":"Space Fiction",
        "Rus":"Космическая фантастика"
    },
    {
        "Key":"sf_social",
        "En":"Socio-psychological fiction",
        "Rus":"Социально-психологическая фантастика"
    },
    {
        "Key":"sf_horror",
        "En":"Horror and Mystery",
        "Rus":"Ужасы и Мистика"
    },
    {
        "Key":"sf_humor",
        "En":"Humorous Fiction",
        "Rus":"Юмористическая фантастика"
    },
    {
        "Key":"sf_fantasy",
        "En":"Fantasy",
        "Rus":"Фэнтези"
    },
    {
        "Key":"sf",
        "En":"Science Fiction",
        "Rus":"Научная Фантастика"
    },
    {
        "Key":"det_classic",
        "En":"Classic Detective",
        "Rus":"Классический детектив"
    },
    {
        "Key":"det_police",
        "En":"Police detective",
        "Rus":"Полицейский детектив"
    },
    {
        "Key":"det_action",
        "En":"Action",
        "Rus":"Боевик"
    },
    {
        "Key":"det_irony",
        "En":"Ironic Detective",
        "Rus":"Иронический детектив"
    },
    {
        "Key":"det_history",
        "En":"Historical detective",
        "Rus":"Исторический детектив"
    },
    {
        "Key":"det_espionage",
        "En":"Spy Detective",
        "Rus":"Шпионский детектив"
    },
    {
        "Key":"det_crime",
        "En":"Crime Detective",
        "Rus":"Криминальный детектив"
    },
    {
        "Key":"det_political",
        "En":"Political Cartoons",
        "Rus":"Политический детектив"
    },
    {
        "Key":"det_maniac",
        "En":"Maniacs",
        "Rus":"Маньяки"
    },
    {
        "Key":"det_hard",
        "En":"Abrupt Detective",
        "Rus":"Крутой детектив"
    },
    {
        "Key":"thriller",
        "En":"Thriller",
        "Rus":"Триллер"
    },
    {
        "Key":"detective",
        "En":"Detective",
        "Rus":"Детектив"
    },
    {
        "Key":"prose_classic",
        "En":"Classical prose",
        "Rus":"Классическая проза"
    },
    {
        "Key":"prose_history",
        "En":"Historical fiction",
        "Rus":"Историческая проза"
    },
    {
        "Key":"prose_contemporary",
        "En":"Modern Prose",
        "Rus":"Современная проза"
    },
    {
        "Key":"prose_counter",
        "En":"Counterculture",
        "Rus":"Контркультура"
    },
    {
        "Key":"prose_rus_classic",
        "En":"Russian classical prose",
        "Rus":"Русская классическая проза"
    },
    {
        "Key":"prose_su_classics",
        "En":"Soviet classical prose",
        "Rus":"Советская классическая проза"
    },
    {
        "Key":"love_contemporary",
        "En":"Modern Romance",
        "Rus":"Современные любовные романы"
    },
    {
        "Key":"love_history",
        "En":"Historical Romance",
        "Rus":"Исторические любовные романы"
    },
    {
        "Key":"love_detective",
        "En":"Action-romance",
        "Rus":"Остросюжетные любовные романы"
    },
    {
        "Key":"love_short",
        "En":"Short Romance",
        "Rus":"Короткие любовные романы"
    },
    {
        "Key":"love_erotica",
        "En":"Erotica",
        "Rus":"Эротика"
    },
    {
        "Key":"adv_western",
        "En":"Western",
        "Rus":"Вестерн"
    },
    {
        "Key":"adv_history",
        "En":"Historical Adventures",
        "Rus":"Исторические приключения"
    },
    {
        "Key":"adv_indian",
        "En":"Adventure about Indians",
        "Rus":"Приключения про индейцев"
    },
    {
        "Key":"adv_maritime",
        "En":"Sea adventures",
        "Rus":"Морские приключения"
    },
    {
        "Key":"adv_geo",
        "En":"Travel and Geography",
        "Rus":"Путешествия и география"
    },
    {
        "Key":"adv_animal",
        "En":"Nature and Animals",
        "Rus":"Природа и животные"
    },
    {
        "Key":"adventure",
        "En":"Adventure",
        "Rus":"Приключения"
    },
    {
        "Key":"child_tale",
        "En":"Fairytale",
        "Rus":"Сказка"
    },
    {
        "Key":"child_verse",
        "En":"Nursery rhymes",
        "Rus":"Детские стихи"
    },
    {
        "Key":"child_prose",
        "En":"Children's prose",
        "Rus":"Детскиая проза"
    },
    {
        "Key":"child_sf",
        "En":"Children's Fiction",
        "Rus":"Детская фантастика"
    },
    {
        "Key":"child_det",
        "En":"Baby Action",
        "Rus":"Детские остросюжетные"
    },
    {
        "Key":"child_adv",
        "En":"Children's Adventures",
        "Rus":"Детские приключения"
    },
    {
        "Key":"child_education",
        "En":"Children's Educational Books",
        "Rus":"Детская образовательная литература"
    },
    {
        "Key":"children",
        "En":"Children's Books",
        "Rus":"Детская литература"
    },
    {
        "Key":"poetry",
        "En":"Poetry",
        "Rus":"Поэзия"
    },
    {
        "Key":"dramaturgy",
        "En":"Drama",
        "Rus":"Драматургия"
    },
    {
        "Key":"antique_ant",
        "En":"Antique Literature",
        "Rus":"Античная литература"
    },
    {
        "Key":"antique_european",
        "En":"European Ancient Literature",
        "Rus":"Европейская старинная литература"
    },
    {
        "Key":"antique_russian",
        "En":"Old Russian literature",
        "Rus":"Древнерусская литература"
    },
    {
        "Key":"antique_east",
        "En":"ancient oriental literature",
        "Rus":"Древневосточная литература"
    },
    {
        "Key":"antique_myths",
        "En":"Myths. Legends. epos",
        "Rus":"Мифы. Легенды. Эпос"
    },
    {
        "Key":"antique",
        "En":"Ancient Literature",
        "Rus":"Старинная литература"
    },
    {
        "Key":"sci_history",
        "En":"History",
        "Rus":"История"
    },
    {
        "Key":"sci_psychology",
        "En":"Psychology",
        "Rus":"Психология"
    },
    {
        "Key":"sci_culture",
        "En":"Cultural",
        "Rus":"Культурология"
    },
    {
        "Key":"sci_religion",
        "En":"Religious",
        "Rus":"Религиоведение"
    },
    {
        "Key":"sci_philosophy",
        "En":"Philosophy",
        "Rus":"Философия"
    },
    {
        "Key":"sci_politics",
        "En":"Politics",
        "Rus":"Политика"
    },
    {
        "Key":"sci_business",
        "En":"Business Books",
        "Rus":"Деловая литература"
    },
    {
        "Key":"sci_juris",
        "En":"Jurisprudence",
        "Rus":"Юриспруденция"
    },
    {
        "Key":"sci_linguistic",
        "En":"Linguistics",
        "Rus":"Языкознание"
    },
    {
        "Key":"sci_medicine",
        "En":"Medicine",
        "Rus":"Медицина"
    },
    {
        "Key":"sci_phys",
        "En":"Physics",
        "Rus":"Физика"
    },
    {
        "Key":"sci_math",
        "En":"Mathematics",
        "Rus":"Математика"
    },
    {
        "Key":"sci_chem",
        "En":"Chemistry",
        "Rus":"Химия"
    },
    {
        "Key":"sci_biology",
        "En":"Biology",
        "Rus":"Биология"
    },
    {
        "Key":"sci_tech",
        "En":"Engineering",
        "Rus":"Технические науки"
    },
    {
        "Key":"science",
        "En":"Nonfiction",
        "Rus":"Научная литература"
    },
    {
        "Key":"comp_www",
        "En":"Internet",
        "Rus":"Интернет"
    },
    {
        "Key":"comp_programming",
        "En":"Programming",
        "Rus":"Программирование"
    },
    {
        "Key":"comp_hard",
        "En":"Hardware",
        "Rus":"Аппаратное обеспечение"
    },
    {
        "Key":"comp_soft",
        "En":"Programs",
        "Rus":"Программы"
    },
    {
        "Key":"comp_db",
        "En":"Databases",
        "Rus":"Базы данных"
    },
    {
        "Key":"comp_osnet",
        "En":"OS and Network",
        "Rus":"ОС и Сети"
    },
    {
        "Key":"computers",
        "En":"computer-literature",
        "Rus":"Компьтерная литература"
    },
    {
        "Key":"ref_encyc",
        "En":"Encyclopedias",
        "Rus":"Энциклопедии"
    },
    {
        "Key":"ref_dict",
        "En":"Dictionaries",
        "Rus":"Словари"
    },
    {
        "Key":"ref_ref",
        "En":"Directories",
        "Rus":"Справочники"
    },
    {
        "Key":"ref_guide",
        "En":"Manuals",
        "Rus":"Руководства"
    },
    {
        "Key":"reference",
        "En":"Reference",
        "Rus":"Справочная литература"
    },
    {
        "Key":"nonf_biography",
        "En":"Biographies and Memoirs",
        "Rus":"Биографии и Мемуары"
    },
    {
        "Key":"nonf_publicism",
        "En":"Reading",
        "Rus":"Публицистика"
    },
    {
        "Key":"nonf_criticism",
        "En":"Criticism",
        "Rus":"Критика"
    },
    {
        "Key":"design",
        "En":"Art and Design",
        "Rus":"Искусство и Дизайн"
    },
    {
        "Key":"nonfiction",
        "En":"Nonfiction",
        "Rus":"Документальная литература"
    },
    {
        "Key":"religion_rel",
        "En":"Religion",
        "Rus":"Религия"
    },
    {
        "Key":"religion_esoterics",
        "En":"Esoteric",
        "Rus":"Эзотерика"
    },
    {
        "Key":"religion_self",
        "En":"Self Improvement",
        "Rus":"Самосовершенствование"
    },
    {
        "Key":"religion",
        "En":"Religionaya literature",
        "Rus":"Религионая литература"
    },
    {
        "Key":"humor_anecdote",
        "En":"Jokes",
        "Rus":"Анекдоты"
    },
    {
        "Key":"humor_prose",
        "En":"Humorous prose",
        "Rus":"Юмористическая проза"
    },
    {
        "Key":"humor_verse",
        "En":"Humorous poems",
        "Rus":"Юмористические стихи"
    },
    {
        "Key":"humor",
        "En":"Humor",
        "Rus":"Юмор"
    },
    {
        "Key":"home_cooking",
        "En":"Cooking",
        "Rus":"Кулинария"
    },
    {
        "Key":"home_pets",
        "En":"Pets",
        "Rus":"Домашние животные"
    },
    {
        "Key":"home_crafts",
        "En":"Hobbies and Crafts",
        "Rus":"Хобби и ремесла"
    },
    {
        "Key":"home_entertain",
        "En":"Entertainment",
        "Rus":"Развлечения"
    },
    {
        "Key":"home_health",
        "En":"Health",
        "Rus":"Здоровье"
    },
    {
        "Key":"home_garden",
        "En":"Gardening",
        "Rus":"Сад и огород"
    },
    {
        "Key":"home_diy",
        "En":"DIY",
        "Rus":"Сделай сам"
    },
    {
        "Key":"home_sport",
        "En":"Sports",
        "Rus":"Спорт"
    },
    {
        "Key":"home_sex",
        "En":"Erotic Sex",
        "Rus":"Эротика, Секс"
    },
    {
        "Key":"home",
        "En":"Housekeeping",
        "Rus":"Домоводство"
    }
];