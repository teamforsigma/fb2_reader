"use strict";

describe('Fabric: fb2Schema', function () {
    var should = chai.should();

    beforeEach(module('fb2.parser'));

    it('can get an instance of fb2Schema', inject(function (fb2Schema) {
        should.exist(fb2Schema);
    }))
});

describe('Test', function () {
    it('test', inject(function ($q) {
        var deferred = $q.defer();
        deferred.resolve("test");
        deferred.promise.then(function (obj) {
            console.log(obj);
        });
        deferred.promise.should.eventually.deep.equal("test");
    }));
});

describe('Service: schemaParser', function () {
    var should = chai.should();

    var rootScope;
    var parser;

    beforeEach(module('fb2.parser'));
    beforeEach(inject(function (schemaParser, $rootScope) {
        parser = schemaParser;
        inject(function ($rootScope) {
            rootScope = $rootScope;
        });
    }));

    afterEach(function () {
        rootScope.$apply();
    });

    it("Should parse with simple schema", function () {
        var testSchemaSimple = {
            "simple-attr": {},
            "simple-attr2": {}
        };

        var xml = "<root><simple-attr>attr1</simple-attr><simple-attr2>attr2</simple-attr2></root>";

        parser.parseXmlWithSchema(xml, testSchemaSimple).should.eventually.deep.equal({
            simpleAttr: "attr1",
            simpleAttr2: "attr2"
        });
    });

    it("Should parse with object", function () {
        var testSchemaObject = {
            "objAttr": {
                content: {
                    "attr1": {},
                    "attr2": {},
                    "attr3": {
                        content: {
                            "attr3_1": {}
                        }
                    }
                }
            }
        };

        var xml = "<root><objAttr><attr1>a1</attr1><attr2>a2</attr2><attr3><attr3_1>a3_1</attr3_1></attr3></objAttr></root>";

        parser.parseXmlWithSchema(xml, testSchemaObject).should.eventually.deep.equal({
            objAttr: {
                attr1: 'a1',
                attr2: 'a2',
                attr3: {
                    attr3_1: 'a3_1'
                }
            }
        });
    });

    it("Should parse with array schema", function () {
        var testSchemaArray = {
            "a": {
                array: true
            }
        };

        var xml = "<root><a>1</a><a>2</a><a>3</a><a>4</a></root>";

        parser.parseXmlWithSchema(xml, testSchemaArray).should.eventually.deep.equal({
            a: ["1", "2", "3", "4"]
        });
    });

    it("Should parse with processors", function () {
        var testSchemaArray = {
            "a": {
                array: true
            }
        };

        var xml = "<root><a>1</a><a>2</a><a>3</a><a>4</a></root>";

        parser.parseXmlWithSchema(xml, testSchemaArray).should.eventually.deep.equal({
            a: ["1", "2", "3", "4"]
        });
    });

    it("Should parse complex object", function () {

        var processor = function (result, obj) {
            return obj.text().split(',');
        };

        var testSchemaComplex = {
            "a": {
                content: {
                    "a": {},
                    "b": {
                        processor: processor
                    }
                }
            },
            "b": {
                content: {
                    "simple-attr": {
                        content: {
                            "nested-simple-attr": {}
                        }
                    }
                }
            },
            "c": {
                array: true,
                content: {
                    "a": {},
                    "b": {
                        array: true,
                        processor: processor
                    }
                }
            }
        };

        var xml = "<root><a><a>a_a</a><b>a,b,c,d,e</b></a><b><simple-attr><nested-simple-attr>nested</nested-simple-attr></simple-attr></b><c><a>a1</a><b>b1,b2</b></c><c><a>a1</a><b>b1</b></c></root>";

        parser.parseXmlWithSchema(xml, testSchemaComplex).should.eventually.deep.equal({
            a: {
                a : "a_a",
                b : ["a", "b", "c", "d", "e"]
            },
            b : {
                simpleAttr: {
                    nestedSimpleAttr: "nested"
                }
            },
            c : [
                {
                    a : "a1",
                    b : [["b1", "b2"]]
                },
                {
                    a : "a1",
                    b : [["b1"]]
                }
            ]
        });
    });

    it("Should parse without fail", function () {
        var testSchemaBad = {
            "a-1-1" : {},
            "a---" : {},
            "c" : {
                content : {
                    "d" : {}
                }
            }
        };

        var xml = "<root><a-1-1></a-1-1><a--->test</a---></root>";

        parser.parseXmlWithSchema(xml, testSchemaBad).should.eventually.deep.equal({
            a11 : "",
            a : "test",
            c : {
                d : ""
            }
        });
    });
});