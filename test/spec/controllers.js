'use strict';

describe('Service: settings', function () {
    var should = chai.should();

    // load the controller's module
    beforeEach(module('settingsHolder'));

    var userSettings;

    beforeEach(inject(function (_userSettings_) {
        userSettings = _userSettings_;
    }));

    it('can get an instance of settingsHolder', function() {
        should.exist(userSettings);
    });

    it('settingsHolder returns settings', function() {
        var settings = userSettings.getSettings();
        settings.should.have.property('fontSize');
        settings.should.have.property('fontFamily');
        settings.should.have.property('textColor');
        settings.should.have.property('backColor');
    })
});

